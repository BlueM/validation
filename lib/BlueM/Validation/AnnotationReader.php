<?php

namespace BlueM\Validation;

/**
 * Static utility class for reading validation annotations.
 *
 * As the annotations are read from source code which is not expected to change
 * during the lifetime of the process, parsed constraint values are cached.
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class AnnotationReader
{
    /**
     * Reads the given property's doc block and extracts all annotations with
     * "validation-" prefix (without the prefix).
     *
     * @param \ReflectionProperty $property
     * @param string              $name     Annotation name, without "validation-"
     *                                      prefix. If it does not exist, returns null.
     * @return mixed
     */
    public static function getConstraint(\ReflectionProperty $property, $name)
    {
        $constraints = static::readAnnotation($property);

        if (array_key_exists($name, $constraints)) {
            return $constraints[$name];
        }

        return null;
    }

    /**
     * Reads the given property's doc block and extracts all annotations with
     * "validation-" prefix (without the prefix).
     *
     * @param \ReflectionProperty $property
     *
     * @return array
     */
    public static function getConstraints(\ReflectionProperty $property)
    {
        return static::readAnnotation($property);
    }

    /**
     * @param \ReflectionProperty $property
     *
     * @return array
     */
    private static function readAnnotation(\ReflectionProperty $property)
    {
        static $constraints = array();

        $propertyKey = $property->getDeclaringClass().'$'.$property->getName();

        if (empty($constraints[$propertyKey])) {
            $constraints[$propertyKey] = array();

            $lines = array_map(
                function ($line) {
                    return ltrim($line, ' *');
                },
                explode("\n", trim($property->getDocComment(), "\r\n\t "))
            );

            foreach ($lines as $line) {
                $tokens = explode(' ', trim($line), 2);

                if ('@validation-' != substr($tokens[0], 0, 12)) {
                    continue;
                }

                $name = substr($tokens[0], 12);

                if (isset($tokens[1]) && '' !== trim($tokens[1])) {
                    $constraints[$propertyKey][$name] = trim($tokens[1]);
                } else {
                    $constraints[$propertyKey][$name] = true;
                }
            }
        }

        return $constraints[$propertyKey];
    }
}

<?php

namespace BlueM\Validation;

/**
 * A ValidationFailedException is thrown either when a constraint is violated or
 * when a transformer encounters unexpected/unparseable input.
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class ValidationFailedWithFieldnameException extends ValidationFailedException
{
}

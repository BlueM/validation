<?php

namespace BlueM\Validation;

/**
 * Abstract base class for all validation constraints
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
abstract class Constraint
{
    /**
     * Checks whether the value given as argument
     *
     * @param mixed       $value
     * @param object|null $object
     * @param array|null  $collectionItem
     *
     * @return
     */
    abstract public function check(
        $value,
        $object = null,
        $collectionItem = null
    );

    /**
     * @return bool
     */
    public function skipsBlankValues()
    {
        return true;
    }
}

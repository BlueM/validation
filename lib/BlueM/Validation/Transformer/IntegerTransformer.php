<?php

namespace BlueM\Validation\Transformer;

use BlueM\Validation\Transformer;
use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\ValidationFailure;

/**
 * Provides transformation of the given (possibly with localized thousands
 * separator) string to a integer
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class IntegerTransformer extends Transformer
{
    /**
     * @var string
     */
    protected $decSep;

    /**
     * @var string
     */
    protected $thsSep;

    /**
     * @param string $decSep
     * @param string $thsSep
     */
    public function __construct($decSep, $thsSep)
    {
        $this->decSep = $decSep;
        $this->thsSep = $thsSep;
    }

    /**
     * {@inheritDoc}
     */
    public function transform($value)
    {
        if ('' === $value || null === $value) {
            // Nothing to transform
            return null;
        }

        if ('' == $this->decSep) {
            $parts = array($value);
        } else {
            $parts = explode($this->decSep, $value, 2);
        }

        $case1 = '\d*';
        $case2 = '-?\d+';
        $case3 = '-?\d{1,3}(?:'.preg_quote($this->thsSep).'\d{3})*';

        if (!preg_match("/^(?:$case1|$case2|$case3)$/", $parts[0])) {
            // Part before the decimal separator (if any) failed
            throw new ValidationFailedException(ValidationFailure::NOT_INT);
        }

        if (isset($parts[1])) {
            if (!preg_match('/^0+$/', $parts[1])) {
                throw new ValidationFailedException(ValidationFailure::NOT_INT);
            }
        }

        return intval(str_replace($this->thsSep, '', $parts[0]));
    }
}

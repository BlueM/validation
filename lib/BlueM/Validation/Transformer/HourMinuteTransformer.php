<?php

namespace BlueM\Validation\Transformer;

use BlueM\Validation\Transformer;
use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\ValidationFailure;

/**
 * Provides transformation from a hour + minute string to an hh:mm representation
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class HourMinuteTransformer extends Transformer
{
    /**
     * @var bool
     */
    protected $acceptPeriod;

    /**
     * @param bool $acceptPeriod
     */
    public function __construct($acceptPeriod)
    {
        $this->acceptPeriod = $acceptPeriod;
    }

    /**
     * {@inheritDoc}
     */
    public function transform($value)
    {
        if ('' === $value || is_null($value)) {
            // Nothing to transform
            return null;
        }

        if (preg_match('#^(?P<hour>\d{2}):(?P<minute>\d{2})$#', $value, $matches)) {
            // Always accept hh::mm
        } else {
            // Use hour and minute as given, with or without leading zero,
            // with or without "am" or "pm" suffix, depending on I18n::TIME_ACCEPT_PERIOD
            $re = '(?P<hour>\d{1,2})(?P<minute>[.:]\d{1,2})?';

            if ($this->acceptPeriod) {
                $re .= '\s*(?P<period>pm|am)?';
            }

            if (!preg_match("/^$re$/i", trim($value), $matches)) {
                throw new ValidationFailedException(ValidationFailure::WRONG_HMFORMAT);
            }

            if (empty($matches['minute'])) {
                $matches['minute'] = 0;
            } else {
                $matches['minute'] = substr($matches['minute'], 1);
            }

            if (!empty($matches['period'])) {
                if ($matches['hour'] > 12) {
                    // 24h time + am or pm does not make sense
                    throw new ValidationFailedException(ValidationFailure::WRONG_HMFORMAT);
                }
                if (strtolower($matches['period']) == 'pm') {
                    $matches['hour'] += 12;
                }
            }
        }

        // If valid: return string
        if (intval($matches['hour'])   >= 0  &&
            intval($matches['hour'])   <= 23 &&
            intval($matches['minute']) >= 0  &&
            intval($matches['minute']) <= 59
        ) {
            // Looks valid
            return sprintf("%02d:%02d", $matches['hour'], $matches['minute']);
        }

        throw new ValidationFailedException(ValidationFailure::WRONG_HMFORMAT);
    }
}

<?php

namespace BlueM\Validation\Transformer;

use BlueM\Validation\Transformer;
use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\ValidationFailure;

/**
 * Provides transformation from a localized date string to Y-m-d representation
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class DateTransformer extends Transformer
{
    /**
     * @var string
     */
    protected $dateInputFormat;

    /**
     * @param string $dateInputFormat
     */
    public function __construct($dateInputFormat)
    {
        $this->dateInputFormat = $dateInputFormat;
    }

    /**
     * {@inheritDoc}
     */
    public function transform($value)
    {
        if ('' === $value || is_null($value)) {
            // Nothing to transform
            return null;
        }

        if (preg_match('#^(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})$#', $value, $matches)) {
            // Always accept Y-m-d
        } else {
            // Use localized date

            // Reminder: we don't use DateTime::createFromFormat() in order to
            // be able to accept the year as 2 or 4 digits
            $re = '';
            for ($i = 0, $ii = strlen($this->dateInputFormat); $i < $ii; $i++) {
                switch ($this->dateInputFormat{$i}) {
                    case 'd':
                    case 'D':
                        $re .= '(?P<day>\d{1,2})';
                        break;
                    case 'm':
                    case 'M':
                        $re .= '(?P<month>\d{1,2})';
                        break;
                    case 'y':
                    case 'Y':
                        $re .= '(?P<year>\d{2,4})';
                        break;
                    default:
                        $re .= preg_quote($this->dateInputFormat{$i}, '/');
                }
            }

            if (!preg_match("/^$re$/", trim($value), $matches)) {
                throw new ValidationFailedException(
                    ValidationFailure::WRONG_DATEFORMAT,
                    $this->dateInputFormat
                );
            }
        }

        $d = $matches['day'];
        $m = $matches['month'];
        $y = $matches['year'];

        if (mb_strlen($y) < 3) {
            $y = sprintf('%02d', $y);
            if ($y > date('y')) {
                throw new ValidationFailedException(
                    ValidationFailure::WRONG_DATEFORMAT,
                    $this->dateInputFormat
                );
            }
            $y = "20$y";
        }

        // If valid: return string
        if (checkdate($m, $d, $y)) {
            return sprintf("%04d-%02d-%02d", $y, $m, $d);
        }

        throw new ValidationFailedException(ValidationFailure::INVALID_DATE);
    }
}

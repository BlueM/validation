<?php

namespace BlueM\Validation\Transformer;

use BlueM\Validation\Transformer;

/**
 * Provides transformation to a boolean value.
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class BoolTransformer extends Transformer
{
    /**
     * {@inheritDoc}
     */
    public function transform($value)
    {
        return !empty($value);
    }
}

<?php

namespace BlueM\Validation\Transformer;

use BlueM\Validation\Transformer;

/**
 * Provides addition of "http://" scheme string to a string, if it starts with "www."
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class AddHTTPSchemeTransformer extends Transformer
{
    /**
     * {@inheritDoc}
     */
    public function transform($value)
    {
        if ('' === $value || is_null($value)) {
            // Nothing to transform
            return $value;
        }

        if ('www.' === substr(strtolower($value), 0, 4)) {
            $value = "http://$value";
        }

        return $value;
    }
}

<?php

namespace BlueM\Validation\Transformer;

use BlueM\Validation\Transformer;

/**
 * Provides Unicode normalization, provided the intl extension is available
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class NormalizeTransformer extends Transformer
{
    /**
     * {@inheritDoc}
     */
    public function transform($value)
    {
    	if (class_exists('Normalizer')) {
    		return \Normalizer::normalize($value);
    	}

        return $value;
    }
}

<?php

namespace BlueM\Validation\Transformer;

use BlueM\Validation\Transformer;

/**
 * Provides normalization of returns/linefeeds to Unix newlines ("\n")
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class NormalizeReturnTransformer extends Transformer
{
    /**
     * {@inheritDoc}
     */
    public function transform($value)
    {
        return str_replace(array("\r\n", "\r"), "\n", $value);
    }
}

<?php

namespace BlueM\Validation\Transformer;

use BlueM\Validation\Transformer;
use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\ValidationFailure;

/**
 * Provides transformation of the given string (possibly with localized decimal and
 * thousands separator(s)) to a float
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class FloatTransformer extends Transformer
{
    /**
     * @var string
     */
    protected $decSep;

    /**
     * @var string
     */
    protected $thsSep;

    /**
     * @param string $decSep
     * @param string $thsSep
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($decSep, $thsSep)
    {
        if (!$decSep) {
            throw new \InvalidArgumentException('The decimal separator may not be empty.');
        }
        $this->decSep = $decSep;
        $this->thsSep = $thsSep;
    }

    /**
     * {@inheritDoc}
     */
    public function transform($value)
    {
        if ('' === $value || null === $value) {
            // Nothing to transform
            return null;
        }

        $parts = explode($this->decSep, $value, 2);

        $case1 = '\d*';
        $case2 = '-?\d+';
        $case3 = '-?\d{1,3}(?:'.preg_quote($this->thsSep).'\d{3})*';

        if (!preg_match("/^(?:$case1|$case2|$case3)$/", $parts[0])) {
            // Part before the decimal separator failed
            throw new ValidationFailedException(ValidationFailure::NOT_NUMBER);
        }

        if (isset($parts[1])) {
            if (!preg_match('/^\d+$/', $parts[1])) {
                throw new ValidationFailedException(ValidationFailure::NOT_NUMBER);
            }
        }

        $parts[0] = str_replace($this->thsSep, '', $parts[0]);

        return (float) join('.', $parts);
    }
}

<?php

namespace BlueM\Validation\Transformer;

use BlueM\Validation\Transformer;
use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\ValidationFailure;

/**
 * Provides transformation from a month (given in one out of various possible formats) to a Y-m representation
 *
 * A value is considered a valid month if it is a string consisting of a month number (1
 * to 12, with or without leading zeros) and a year (positive 4-digit number), separated
 * by [./ -], in any order (year-month or month-year). Additionally, the year has to be
 * inside the (arbitrarily defined) range of 1900..2100
 *
 * Valid input values for months are: "02/2013", "2009.07", "03-2014" etc.
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class MonthTransformer extends Transformer
{
    /**
     * {@inheritDoc}
     */
    public function transform($value)
    {
        if ('' == $value || is_null($value)) {
            // Nothing to transform
            return null;
        }

        if (!preg_match('#^(\d{1,4})[./ -](\d{1,4})$#', $value, $matches)) {
            // Does not look like a supported format
            throw new ValidationFailedException(ValidationFailure::WRONG_MONTHFORMAT);
        }

        if (4 === strlen($matches[1])) {
            list(, $y, $m) = $matches;
        } elseif (4 === strlen($matches[2])) {
            list(, $m, $y) = $matches;
        } else {
            throw new ValidationFailedException(ValidationFailure::WRONG_MONTHFORMAT);
        }

        if ($m < 1 || $m > 12) {
            throw new ValidationFailedException(ValidationFailure::WRONG_MONTHFORMAT);
        }

        if ($y < 1900 || $y > 2100) {
            throw new ValidationFailedException(ValidationFailure::WRONG_MONTHFORMAT);
        }

        // Looks valid
        return sprintf('%d-%02d', $y, $m);
    }
}

<?php

namespace BlueM\Validation\I18n;

use BlueM\Validation\I18n;
use BlueM\Validation\ValidationFailure;

/**
 * Provides the German localization
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class De extends I18n
{
    /**
     * @var string
     */
    protected $decimalSeparator = ',';

    /**
     * @var string
     */
    protected $thousandsSeparator = '.';

    /**
     * @var string
     */
    protected $dateFormat = 'd.m.Y';

    /**
     * {@inheritDoc}
     */
    public function acceptsPeriod()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    protected function getStrings()
    {
        return array(
            ValidationFailure::BLANK             => 'Dieses Feld muss leer sein.',
            ValidationFailure::BLANK_IF          => 'Dieses Feld muss leer sein, wenn das Feld %s nicht leer ist.',
            ValidationFailure::BLANK_IFNOT       => 'Wenn das Feld %s leer ist, muss dieses Feld ebenfalls leer sein.',
            ValidationFailure::FAIL_GENERIC      => 'Die Eingabe ist ungültig.',
            ValidationFailure::INVALID_DATE      => 'Das Datum ist ungültig.',
            ValidationFailure::MANDATORY         => 'Dieser Wert ist erforderlich.',
            ValidationFailure::MANDATORY_IF      => 'Dieser Wert ist erforderlich, wenn das Feld %s nicht leer ist.',
            ValidationFailure::MANDATORY_IFNOT   => 'Dieses Feld oder das Feld %s muss eingegeben werden.',
            ValidationFailure::NOT_EMAIL         => 'Dies ist keine gültige E-Mail-Adresse.',
            ValidationFailure::NOT_INT           => 'Dies muss eine Ganzzahl sein.',
            ValidationFailure::NOT_INVALUELIST   => 'Dieser Wert passt nicht zur vorgegebenen Werteliste.',
            ValidationFailure::NOT_MATCHPATTERN  => 'Der Wert ist formal ungültig.',
            ValidationFailure::NOT_NUMBER        => 'Dies muss eine Zahl sein.',
            ValidationFailure::NOT_URL           => 'Dies ist keine gültige WWW-Adresse (URL).',
            ValidationFailure::TOO_FEW           => 'Es dürfen nicht weniger als %d Einträge vorhanden sein.',
            ValidationFailure::TOO_LARGE         => 'Der Wert darf nicht größer sein als %s.',
            ValidationFailure::TOO_LONG          => 'Der Wert ist %d Zeichen länger als erlaubt.',
            ValidationFailure::TOO_MANYDECPLACES => 'Es sind maximal %d Nachkommastellen erlaubt.',
            ValidationFailure::TOO_SHORT         => 'Der Wert muss mindestens %d Zeichen lang sein.',
            ValidationFailure::TOO_SMALL         => 'Der Wert darf nicht kleiner sein als %s.',
            ValidationFailure::WRONG_DATEFORMAT  => 'Das Datum wird im Format TT.MM.[JJ]JJ erwartet.',
            ValidationFailure::WRONG_MONTHFORMAT => 'Der Monat wird als numerischer Wert (z.B. „5“ für „Mai“) erwartet, der durch . oder / oder - oder Leerzeichen vom Jahr getrennt ist.',
            ValidationFailure::WRONG_HMFORMAT    => "Stunde und Minute werden im Format HH[:MM] erwartet.",
            ValidationFailure::WRONG_TYPE        => 'Ungültiger Datentyp.',
            ValidationFailure::NOT_WELLFORMED    => 'Dies ist kein gültiges XML.',
            ValidationFailure::NOT_JSON          => 'Dies ist kein gültiges JSON.',
        );
    }
}

<?php

namespace BlueM\Validation\I18n;

use BlueM\Validation\I18n;
use BlueM\Validation\ValidationFailure;

/**
 * Provides the English localization
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class En extends I18n
{
    /**
     * {@inheritDoc}
     */
    public function acceptsPeriod()
    {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    protected function getStrings()
    {
        return array(
            ValidationFailure::BLANK             => 'This value must be empty.',
            ValidationFailure::BLANK_IF          => 'This field must be left empty, if %s is not empty.',
            ValidationFailure::BLANK_IFNOT       => 'If %s is empty, this field must be empty, too.',
            ValidationFailure::FAIL_GENERIC      => 'This value is invalid.',
            ValidationFailure::INVALID_DATE      => 'The date is invalid.',
            ValidationFailure::MANDATORY         => 'This value is mandatory.',
            ValidationFailure::MANDATORY_IF      => 'This value is mandatory, if field %s is not empty.',
            ValidationFailure::MANDATORY_IFNOT   => 'This field and field %s may not both be empty.',
            ValidationFailure::NOT_EMAIL         => 'This is not a valid e-mail address.',
            ValidationFailure::NOT_INT           => 'This must be an integer.',
            ValidationFailure::NOT_INVALUELIST   => 'This value is not in the list of allowed values.',
            ValidationFailure::NOT_MATCHPATTERN  => 'The value is formally invalid.',
            ValidationFailure::NOT_NUMBER        => 'This must be a number.',
            ValidationFailure::NOT_URL           => 'This is not a valid WWW address (URL).',
            ValidationFailure::TOO_FEW           => 'At least %d values are required.',
            ValidationFailure::TOO_LARGE         => 'The value must not be larger than %s.',
            ValidationFailure::TOO_LONG          => 'The value contains %d characters more than allowed.',
            ValidationFailure::TOO_MANYDECPLACES => 'The number may not have more than %d decimal places.',
            ValidationFailure::TOO_SHORT         => 'The value must not have less than %d characters.',
            ValidationFailure::TOO_SMALL         => 'The value must not be smaller than %s.',
            ValidationFailure::WRONG_DATEFORMAT  => 'The date is expected in MM/DD/[YY]YY format.',
            ValidationFailure::WRONG_HMFORMAT    => "Hour and minute are expected in HH[:MM] format, either with “am”/“pm” suffix or as 24-hour time.",
            ValidationFailure::WRONG_MONTHFORMAT => 'The month is expected as numerical value (e.g. “5” for “May”), separated from the year by . or / or - or space.',
            ValidationFailure::WRONG_TYPE        => 'Invalid data type.',
            ValidationFailure::NOT_WELLFORMED    => 'This is not well-formed XML.',
            ValidationFailure::NOT_JSON          => 'This is not valid JSON.',
        );
    }
}

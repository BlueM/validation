<?php

namespace BlueM\Validation;

/**
 * Thrown when an argument is a collection which fails validation.
 */
class InvalidCollectionArgumentException extends \InvalidArgumentException
{
    /**
     * @var array
     */
    protected $errors;

    /**
     * @param string $message Exception message
     * @param array  $errors  Associative array of property name => error message pairs
     */
    public function __construct($message, array $errors)
    {
        parent::__construct($message);
        $this->errors = $errors;
    }

    /**
     * Returns the validation errors
     *
     * @return array Associative array of name => message pairs
     */
    public function getErrors()
    {
        return $this->errors;
    }
}

<?php

namespace BlueM\Validation\Constraint;

use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\ValidationFailure;

/**
 * Makes sure that the given value is not smaller (whatever that means,
 * depending on the datatype) as the argument given to the constructor.
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class Minimum extends AbstractLimitConstraint
{
    /**
     * @var mixed
     */
    protected $min;

    /**
     * @param mixed $min
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($min)
    {
        if (!is_scalar($min)) {
            // Note: the minimum can not only be an integer,
            // but also a float or even a string
            throw new \InvalidArgumentException('Invalid minimum value given');
        }

        $this->min = $min;
    }

    /**
     * {@inheritDoc}
     */
    public function check(
        $value,
        $object = null,
        $collectionItem = null
    ) {
        $this->min = $this->getLimit($this->min, $object, $collectionItem);

        if (!is_null($this->min) && $value < $this->min) {
            throw new ValidationFailedException(ValidationFailure::TOO_SMALL, $this->min);
        }

        return $value;
    }
}

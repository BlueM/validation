<?php

namespace BlueM\Validation\Constraint;

use BlueM\Validation\Constraint;
use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\ValidationFailure;

/**
 * Provides the Pattern constraint
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class Pattern extends Constraint
{
    /**
     * @var string
     */
    protected $pattern;

    /**
     * @var int
     */
    protected $code;

    /**
     * Constructor
     *
     * @param string $pattern The regular expression, including delimiters and modifiers
     * @param int    $code    Exception code
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($pattern, $code = 0)
    {
        if (!is_string($pattern)) {
            throw new \InvalidArgumentException('Invalid pattern given');
        }
        $this->pattern = $pattern;

        if (!preg_match('/^\d+$/', $code)) {
            throw new \InvalidArgumentException('Invalid exception code given');
        }

        $this->code = $code ? $code : ValidationFailure::NOT_MATCHPATTERN;
    }

    /**
     * {@inheritDoc}
     */
    public function check(
        $value,
        $object = null,
        $collectionItem = null
    ) {
        if (!preg_match($this->pattern, $value)) {
            throw new ValidationFailedException($this->code);
        }

        return $value;
    }
}

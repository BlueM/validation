<?php

namespace BlueM\Validation\Constraint;

use BlueM\Validation\Constraint;
use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\ValidationFailure;

/**
 * Verifies that the given value is scalar or null
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class Scalar extends Constraint
{
    /**
     * {@inheritDoc}
     */
    public function check(
        $value,
        $object = null,
        $collectionItem = null
    ) {
        if (!is_scalar($value) && !is_null($value)) {
            throw new ValidationFailedException(ValidationFailure::WRONG_TYPE);
        }

        return $value;
    }
}

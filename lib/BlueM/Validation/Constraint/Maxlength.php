<?php

namespace BlueM\Validation\Constraint;

use BlueM\Validation\Constraint;
use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\ValidationFailure;

/**
 * Makes sure that a value contains no more than a certain number of characters
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class Maxlength extends Constraint
{
    /**
     * @var int
     */
    protected $length;

    /**
     * @param int $length Maximum length in characters
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($length)
    {
        if (intval($length) < 1) {
            throw new \InvalidArgumentException('Invalid maximum length given');
        }
        $this->length = $length;
    }

    /**
     * {@inheritDoc}
     */
    public function check(
        $value,
        $object = null,
        $collectionItem = null
    ) {
        if (function_exists('mb_strlen')) {
            $diff = mb_strlen($value) - $this->length;
        } else {
            $diff = strlen($value) - $this->length;
        }

        if ($diff > 0) {
            throw new ValidationFailedException(ValidationFailure::TOO_LONG, $diff);
        }

        return $value;
    }
}

<?php

namespace BlueM\Validation\Constraint;

use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\ValidationFailure;

/**
 * Makes sure that the given value is at most as "large" (whatever that means,
 * depending on the datatype) as the argument given to the constructor.
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class Maximum extends AbstractLimitConstraint
{
    /**
     * @var mixed
     */
    protected $max;

    /**
     * @param mixed $max
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($max)
    {
        if (!is_scalar($max)) {
            // Note: the minimum can not only be an integer,
            // but also a float or even a string
            throw new \InvalidArgumentException('Invalid maximum value given');
        }
        $this->max = $max;
    }

    /**
     * {@inheritDoc}
     */
    public function check(
        $value,
        $object = null,
        $collectionItem = null
    ) {
        $this->max = $this->getLimit($this->max, $object, $collectionItem);

        if (!is_null($this->max) && $value > $this->max) {
            throw new ValidationFailedException(ValidationFailure::TOO_LARGE, $this->max);
        }

        return $value;
    }
}

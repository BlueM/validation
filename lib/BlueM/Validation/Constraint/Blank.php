<?php

namespace BlueM\Validation\Constraint;

use BlueM\Validation\Constraint;
use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\ValidationFailure;

/**
 * Makes sure that the given value is null or an empty string or an empty array, either
 * always or under certain circumstances (depending on another property).
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class Blank extends AbstractEmptyConstraint
{
    /**
     * @var string
     */
    protected $constraintCondition;

    /**
     * @param string|null $constraintValue Value for the constraint
     */
    public function __construct($constraintValue = null)
    {
        $this->constraintCondition = $constraintValue;
    }

    /**
     * {@inheritDoc}
     */
    public function check(
        $value,
        $object = null,
        $collectionItem = null
    ) {
        if ($this->constraintCondition && true !== $this->constraintCondition) {
            return $this->checkCondition($value, $object, $collectionItem);
        }

        if (!$this->isBlank($value)) {
            throw new ValidationFailedException(ValidationFailure::BLANK);
        }

        return $value;
    }

    /**
     * @param mixed      $value          The value to check
     * @param object     $object
     * @param array|null $collectionItem
     *
     * @throws \BlueM\Validation\ValidationFailedException
     * @throws \RuntimeException
     * @return mixed
     */
    protected function checkCondition($value, $object, $collectionItem)
    {
        if ($this->isBlank($value)) {
            // Value is empty, nothing to validate
            return $value;
        }

        if (!preg_match(
                '#^IF\s+(NOT\s+)?\$(\$*[a-zA-Z_][a-zA-Z_0-9]*)$#',
                $this->constraintCondition,
                $matches
            )
        ) {
            throw new \RuntimeException(
                'Invalid condition specification “'.$this->constraintCondition.'”'
            );
        }

        list(, $negated, $referenceProperty) = $matches;

        list($comparisonValue, $label) = $this->getReferenceValue($referenceProperty, $object, $collectionItem);

        if ($negated) {
            // Must be blank if the reference value is blank
            if ($this->isBlank($comparisonValue)) {
                throw new ValidationFailedException(ValidationFailure::BLANK_IFNOT, $label);
            }

            return $value;
        }

        // Must be blank if the reference value is not blank
        if (!$this->isBlank($comparisonValue)) {
            throw new ValidationFailedException(ValidationFailure::BLANK_IF, $label);
        }

        return $value;
    }

    /**
     * {@inheritDoc}
     */
    public function skipsBlankValues()
    {
        return false;
    }
}

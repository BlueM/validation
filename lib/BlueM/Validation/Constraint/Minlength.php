<?php

namespace BlueM\Validation\Constraint;

use BlueM\Validation\Constraint;
use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\ValidationFailure;

/**
 * Makes sure that the given string contains at least a minimum number of characters
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class Minlength extends Constraint
{
    /**
     * @var int
     */
    protected $length;

    /**
     * @param int $length Minimum length in characters
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($length)
    {
        if (intval($length) < 1) {
            throw new \InvalidArgumentException('Invalid minimum length given');
        }
        $this->length = $length;
    }

    /**
     * {@inheritDoc}
     */
    public function check(
        $value,
        $object = null,
        $collectionItem = null
    ) {
        if (function_exists('mb_strlen')) {
            if (mb_strlen($value) < $this->length) {
                throw new ValidationFailedException(
                    ValidationFailure::TOO_SHORT,
                    $this->length
                );
            }
        } else {
            if (strlen($value) < $this->length) {
                throw new ValidationFailedException(
                    ValidationFailure::TOO_SHORT,
                    $this->length
                );
            }
        }

        return $value;
    }
}

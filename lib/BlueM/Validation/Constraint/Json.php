<?php

namespace BlueM\Validation\Constraint;

use BlueM\Validation\Constraint;
use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\ValidationFailure;

/**
 * Verifies that the given value (expected to be a string) is valid JSON by trying
 * to decode it
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class Json extends Constraint
{
    /**
     * {@inheritDoc}
     */
    public function check(
        $value,
        $object = null,
        $collectionItem = null
    )
    {
        json_decode($value);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new ValidationFailedException(ValidationFailure::NOT_JSON);
        }

        return $value;
    }
}

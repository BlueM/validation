<?php

namespace BlueM\Validation\Constraint;

use BlueM\Validation\Constraint;
use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\ValidationFailure;

/**
 * Makes sure that the given value (expected to be a float) has no more than a given
 * number of decimal places (digits after the decimal separator)
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class Decimalplaces extends Constraint
{
    /**
     * @var int
     */
    protected $decpl;

    /**
     * @param int $decpl
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($decpl)
    {
        $decpl = intval($decpl);

        if ($decpl < 1) {
            throw new \InvalidArgumentException('Invalid number of decimal places');
        }

        $this->decpl = $decpl;
    }

    /**
     * {@inheritDoc}
     * @throws \InvalidArgumentException
     */
    public function check(
        $value,
        $object = null,
        $collectionItem = null
    ) {
        if (!is_float($value)) {
            throw new \InvalidArgumentException('Expected value to be a float');
        }

        $parts = explode('.', strval($value));
        if (count($parts) > 1) {
            $parts[1] = rtrim($parts[1], '0');
            if (strlen($parts[1]) > $this->decpl) {
                throw new ValidationFailedException(
                    ValidationFailure::TOO_MANYDECPLACES, $this->decpl
                );
            }
        }

        return $value;
    }
}

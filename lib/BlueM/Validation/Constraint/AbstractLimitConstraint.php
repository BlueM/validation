<?php

namespace BlueM\Validation\Constraint;

use BlueM\Validation\Constraint;

/**
 * Abstract base class for constraints that can reference another instance variable or
 * collection item as a value for an upper or lower limit.
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
abstract class AbstractLimitConstraint extends Constraint
{
    /**
     * Checks the value given as upper or lower value limit for a minimum or maximum constraint
     * and returns the actual value for it.
     *
     * As minimum and maximum can use other instance variables or values inside an ArrayCollection
     * as boundary value, this method reads the current value from the instance variable or the
     * ArrayCollection and returns it. Otherwise, the literal value is returned.
     *
     * @param string $referenceValue The (literal or reference) boundary value
     * @param object $object
     * @param array  $collectionItem
     *
     * @return mixed
     */
    protected function getLimit($referenceValue, $object, $collectionItem)
    {
        if (!preg_match('#^\$(\$?[a-z_][a-z0-9_]*)$#i', $referenceValue, $matches)) {
            // Looks like a literal value
            return $referenceValue;
        }

        // Minimum seems to be a property or a value in the same collection item
        if ('$' === substr($matches[1], 0, 1)) {
            // Collection item

            $collectionKey = substr($matches[1], 1);

            if (!is_array($collectionItem)) {
                throw new \RuntimeException(
                    "A constraint of type ".get_class($this)." references a key '$collectionKey' in a non-collection"
                );
            }

            if (!array_key_exists($collectionKey, $collectionItem)) {
                throw new \RuntimeException(
                    "A constraint of type ".get_class($this)." references a non-existing key '$collectionKey' in collection"
                );
            }

            return $collectionItem[$collectionKey];
        }

        try {
            $property = new \ReflectionProperty($object, $matches[1]);
            $property->setAccessible(true);

            return $property->getValue($object);
        } catch (\ReflectionException $e) {
            throw new \RuntimeException(
                "A constraint of type ".get_class($this)." references a non-existing property $matches[1]"
            );
        }
    }
}

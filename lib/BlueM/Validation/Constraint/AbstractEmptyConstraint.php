<?php

namespace BlueM\Validation\Constraint;

use BlueM\Validation\Constraint;

/**
 * Abstract base class for constraints that depend on whether other instance variables or
 * collection items are blank or not blank
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
abstract class AbstractEmptyConstraint extends Constraint
{
    /**
     * @param string     $referenceValue
     * @param object     $object
     * @param array|null $collectionItem
     *
     * @return array
     */
    protected function getReferenceValue($referenceValue, $object, $collectionItem)
    {
        if ('$' === substr($referenceValue, 0, 1)) {
            // Reference value seems to be a property or a value in the same collection item

            $collectionKey = substr($referenceValue, 1);

            if (!is_array($collectionItem)) {
                throw new \RuntimeException(
                    "A constraint of type ".get_class($this)." references a key '$collectionKey' in a non-collection"
                );
            }

            if (!array_key_exists($collectionKey, $collectionItem)) {
                throw new \RuntimeException(
                    "A constraint of type ".get_class($this)." references a non-existing key '$collectionKey' in collection"
                );
            }

            $comparisonValue = $collectionItem[$collectionKey];

            return array($comparisonValue, '$$'.$collectionKey);
        }

        // Reference value is another property
        try {
            $reflectionClass = new \ReflectionClass($object);
            $property        = $reflectionClass->getProperty($referenceValue);
        } catch (\ReflectionException $e) {
            throw new \RuntimeException(
                "A constraint of type ".get_class($this)." references a non-existing property $referenceValue"
            );
        }

        $property->setAccessible(true);
        $comparisonValue = $property->getValue($object);

        return array($comparisonValue, '$'.$referenceValue);
    }

    /**
     * @param mixed $value
     *
     * @return bool
     */
    protected function isBlank($value)
    {
        if ('' === $value) {
            return true;
        }

        if (null === $value) {
            return true;
        }

        if (array() === $value) {
            return true;
        }

        return false;
    }
}

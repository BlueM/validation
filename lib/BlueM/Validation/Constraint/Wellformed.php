<?php

namespace BlueM\Validation\Constraint;

use BlueM\Validation\Constraint;
use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\ValidationFailure;

/**
 * Verifies that the given value (expected to be a string) is well-formed XML
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class Wellformed extends Constraint
{
    /**
     * {@inheritDoc}
     */
    public function check(
        $value,
        $object = null,
        $collectionItem = null
    ) {
        libxml_clear_errors();
        $oldSetting = libxml_use_internal_errors(true);

        $doc = new \DOMDocument();
        $doc->loadXML($value);

        $error = libxml_get_last_error();
        libxml_use_internal_errors($oldSetting);

        if ($error) {
            throw new ValidationFailedException(ValidationFailure::NOT_WELLFORMED);
        }

        return $value;
    }
}

<?php

namespace BlueM\Validation\Constraint;

use BlueM\Validation\Constraint;
use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\ValidationFailedWithFieldnameException;
use BlueM\Validation\ValidationFailure;

/**
 * Makes sure that the given value is not null or an empty string or an empty array
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class Mandatory extends AbstractEmptyConstraint
{
    /**
     * @var string
     */
    protected $constraintCondition;

    /**
     * @param string|null $constraintValue Value for the constraint
     */
    public function __construct($constraintValue = null)
    {
        $this->constraintCondition = $constraintValue;
    }

    /**
     * {@inheritDoc}
     */
    public function check(
        $value,
        $object = null,
        $collectionItem = null
    ) {
        if ($this->constraintCondition && true !== $this->constraintCondition) {
            // There is a dependency defined
            return $this->checkCondition($value, $object, $collectionItem);
        }

        if ($this->isBlank($value)) {
            throw new ValidationFailedException(ValidationFailure::MANDATORY);
        }

        return $value;
    }

    /**
     * @param mixed      $value          The value to check
     * @param object     $object
     * @param array|null $collectionItem
     *
     * @throws \RuntimeException
     * @throws \BlueM\Validation\ValidationFailedException
     * @return mixed
     */
    protected function checkCondition($value, $object, $collectionItem)
    {
        if ('' !== $value && null !== $value) {
            // Value not empty, nothing to do
            return $value;
        }

        $regExp = '#^IF\s+(NOT\s+)?\$(\$*[a-zA-Z_][a-zA-Z_0-9]*)$#';

        if (!preg_match($regExp, $this->constraintCondition, $matches)) {
            throw new \RuntimeException(
                'Invalid condition specification “'.$this->constraintCondition.'”'
            );
        }

        list(, $negated, $referenceProperty) = $matches;

        list($comparisonValue, $label) = $this->getReferenceValue(
            $referenceProperty,
            $object,
            $collectionItem
        );

        if ($negated) {
            // NOT
            if ($this->isBlank($comparisonValue)) {
                throw new ValidationFailedWithFieldnameException(
                    ValidationFailure::MANDATORY_IFNOT, $label
                );
            }

            return $value;
        }

        if (!$this->isBlank($comparisonValue)) {
            throw new ValidationFailedWithFieldnameException(
                ValidationFailure::MANDATORY_IF,
                $label
            );
        }

        return $value;
    }

    /**
     * {@inheritDoc}
     */
    public function skipsBlankValues()
    {
        return false;
    }
}

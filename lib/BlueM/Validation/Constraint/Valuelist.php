<?php

namespace BlueM\Validation\Constraint;

use BlueM\Validation\Constraint;
use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\ValidationFailure;

/**
 * Makes sure that a value is one of the values in a value-list
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class Valuelist extends Constraint
{
    /**
     * @var array
     */
    protected $valuelist;

    /**
     * @param string|array $valueListSpec
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($valueListSpec)
    {
        if (is_array($valueListSpec)) {
            $this->valuelist = $valueListSpec;
        } elseif (is_string($valueListSpec)) {
            $this->valuelist = preg_split('#\s+#', trim($valueListSpec));
        } else {
            throw new \InvalidArgumentException('Invalid value list specification');
        }
    }

    /**
     * {@inheritDoc}
     * @throws \InvalidArgumentException
     */
    public function check(
        $value,
        $object = null,
        $collectionItem = null
    ) {
        if (2 > count($this->valuelist)) {
            throw new \InvalidArgumentException(
                'Valuelist definition contains less than 2 values'
            );
        }

        // Possible future improvements: Use class constants and JSON-defined values
        if (in_array($value, $this->valuelist)) {
            return $value;
        }

        throw new ValidationFailedException(ValidationFailure::NOT_INVALUELIST);
    }
}

<?php

namespace BlueM\Validation;

/**
 * Abstract base class for all transformers
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
abstract class Transformer
{
    /**
     * @param $value
     */
    abstract public function transform($value);
}

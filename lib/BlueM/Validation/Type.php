<?php

namespace BlueM\Validation;

/**
 * Abstract base class for all types
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
abstract class Type
{
    /**
     * @var array
     */
    protected $steps = array();

    /**
     * @param Transformer $transformer
     */
    protected function addTransformer(Transformer $transformer)
    {
        $this->steps[strtolower(get_class($transformer))] = $transformer;
    }

    /**
     * @param Constraint $constraint
     */
    public function addConstraint(Constraint $constraint)
    {
        $this->steps[strtolower(get_class($constraint))] = $constraint;
    }

    /**
     * Performs the actual validation using the Transformers and Constraints
     * specified by concrete subclasses.
     *
     * @param mixed       $value
     * @param object|null $object
     * @param array|null  $collectionItem
     *
     * @return mixed
     */
    public function validate(
        $value,
        $object = null,
        $collectionItem = null
    ) {
        foreach (array_values($this->steps) as $step) {
            if ($step instanceof Transformer) {
                $value = $step->transform($value);
            } else {
                if ((null === $value || '' === $value) && $step->skipsBlankValues()) {
                    continue;
                }
                $value = $step->check($value, $object, $collectionItem);
            }
        }

        return $value;
    }

    /**
     * @param I18n                      $i18n
     * @param ValidationFailedException $exc
     * @param array                     $labels
     *
     * @return string
     */
    public function getExceptionMessage(
        I18n $i18n,
        ValidationFailedException $exc,
        array $labels = array()
    ) {
        $placeHolderValue = $exc->getPlaceholderValue();

        if ($exc instanceof ValidationFailedWithFieldnameException) {
            if (!empty($labels[$placeHolderValue])) {
                $placeHolderValue = $labels[$placeHolderValue];
            }
        }

        return $i18n->getString($exc->getCode(), $placeHolderValue);
    }
}

<?php

namespace BlueM\Validation;

/**
 * Holds constants that determine the type of validation failure
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class ValidationFailure
{
    const BLANK             = 5;
    const BLANK_IF          = 6;
    const BLANK_IFNOT       = 12;
    const FAIL_GENERIC      = 23;
    const INVALID_DATE      = 14;
    const MANDATORY         = 4;
    const MANDATORY_IF      = 9;
    const MANDATORY_IFNOT   = 10;
    const NOT_EMAIL         = 18;
    const NOT_INT           = 16;
    const NOT_JSON          = 26;
    const NOT_INVALUELIST   = 3;
    const NOT_MATCHPATTERN  = 17;
    const NOT_NUMBER        = 7;
    const NOT_URL           = 19;
    const NOT_WELLFORMED    = 25;
    const TOO_FEW           = 11;
    const TOO_LARGE         = 2;
    const TOO_LONG          = 20;
    const TOO_MANYDECPLACES = 15;
    const TOO_SHORT         = 21;
    const TOO_SMALL         = 1;
    const WRONG_DATEFORMAT  = 13;
    const WRONG_HMFORMAT    = 22;
    const WRONG_MONTHFORMAT = 24;
    const WRONG_TYPE        = 8;
}

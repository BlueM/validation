<?php

namespace BlueM\Validation\Type;

use BlueM\Validation\Constraint;
use BlueM\Validation\Transformer;
use BlueM\Validation\Type;

/**
 * Defines the "Month" validation type
 *
 * For valid input formats, see Transformer\Month
 * A month that passes validation will be returned in "Y-m" format, e.g. "2012-11"
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class MonthType extends Type
{
    /**
     * Constructor. Sets the constraints and transformers for the type
     */
    public function __construct()
    {
        $this->addConstraint(new Constraint\Scalar());

        $this->addTransformer(new Transformer\NormalizeReturnTransformer());

        $this->addTransformer(new Transformer\SingleLineTransformer());

        $this->addTransformer(new Transformer\TrimTransformer());

        $this->addTransformer(new Transformer\MonthTransformer());
    }
}

<?php

namespace BlueM\Validation\Type;

use BlueM\Validation\Constraint;
use BlueM\Validation\Transformer;
use BlueM\Validation\Transformer\NormalizeTransformer;
use BlueM\Validation\Transformer\NormalizeReturnTransformer;
use BlueM\Validation\Transformer\SingleLineTransformer;
use BlueM\Validation\Transformer\TrimTransformer;
use BlueM\Validation\Type;

/**
 * Defines the String validation type
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class StringType extends Type
{
    /**
     * Constructor. Sets the constraints and transformers for the type
     */
    public function __construct()
    {
        $this->addConstraint(new Constraint\Scalar());

        $this->addTransformer(new NormalizeReturnTransformer());

        $this->addTransformer(new SingleLineTransformer());

        $this->addTransformer(new TrimTransformer());

        $this->addTransformer(new NormalizeTransformer());

        $this->addConstraint(new Constraint\Maxlength(250));
    }
}

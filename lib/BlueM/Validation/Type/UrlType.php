<?php

namespace BlueM\Validation\Type;

use BlueM\Validation\Constraint\Maxlength;
use BlueM\Validation\Constraint\Pattern;
use BlueM\Validation\Constraint\Scalar;
use BlueM\Validation\Transformer\AddHTTPSchemeTransformer;
use BlueM\Validation\Transformer\TrimTransformer;
use BlueM\Validation\Type;
use BlueM\Validation\ValidationFailure;

/**
 * Defines the Url validation type
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class UrlType extends Type
{
    /**
     * Constructor. Sets the constraints and transformers for the type
     */
    public function __construct()
    {
        $this->addConstraint(new Scalar());

        $this->addTransformer(new TrimTransformer());

        $this->addTransformer(new AddHTTPSchemeTransformer());

        $this->addConstraint(new Maxlength(150));

        $re = '#^https?://'.                   // Scheme
              '(?:(?:(?:[\w-]+\.)+[a-z]{2,6}'. // Hostname
              '(?::\d{2,5})?))'.               // Optionally: port
              '(/\S*)?$#i';                    // The rest
        $this->addConstraint(new Pattern($re, ValidationFailure::NOT_URL));
    }
}

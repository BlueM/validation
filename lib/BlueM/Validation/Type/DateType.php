<?php

namespace BlueM\Validation\Type;

use BlueM\Validation\Transformer\DateTransformer;
use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\Constraint\Scalar;
use BlueM\Validation\Transformer\TrimTransformer;
use BlueM\Validation\Type;
use BlueM\Validation\I18n;

/**
 * Defines the Date validation type
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class DateType extends Type
{
    /**
     * @param I18n $i18n
     * @param bool $localized
     */
    public function __construct(I18n $i18n, $localized)
    {
        $this->addConstraint(new Scalar());

        $this->addTransformer(new TrimTransformer());

        $this->addTransformer(
            new DateTransformer($localized ? $i18n->getDateFormat() : '')
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getExceptionMessage(
        I18n $i18n,
        ValidationFailedException $exc,
        array $labels = array()
    ) {
        $value = date($i18n->getDateFormat(), strtotime($exc->getPlaceholderValue()));

        return $i18n->getString($exc->getCode(), $value);
    }
}

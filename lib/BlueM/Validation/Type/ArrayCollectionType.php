<?php

namespace BlueM\Validation\Type;

use BlueM\Validation\InvalidCollectionArgumentException;
use BlueM\Validation\Type;

/**
 * Type which consists of a collection (indexed array) of associative arrays,
 * where the latter ones have certain constraints
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class ArrayCollectionType extends AbstractCollectionType
{
    /**
     * {@inheritDoc}
     */
    public function validate(
        $arrayOfArrays,
        $object = null,
        $collectionItem = null,
        array $labels = null
    ) {
        $this->checkOuterArray($arrayOfArrays);

        // Re-structure the constraints so that the collection items are keys
        // in an associative array of constraints.
        $itemConstraints      = array();
        foreach ($this->constraints as $k => $v) {
            $parts = explode(':', $k, 2);
            if (2 !== count($parts)) {
                continue;
            }
            if (empty($itemConstraints[$parts[1]])) {
                $itemConstraints[$parts[1]] = array();
            }
            $itemConstraints[$parts[1]][$parts[0]] = $v;
        }

        $errors        = array();
        $arrayOfArrays = array_values($arrayOfArrays);

        $expectedFields = array_keys($itemConstraints);

        for ($i = 0, $ii = count($arrayOfArrays); $i < $ii; $i++) {
            if (!is_array($arrayOfArrays[$i])) {
                throw new \InvalidArgumentException("Item $i is not an array");
            }

            // Find fields for which a constraint is defined, but which are not
            // present, and add them so that they are validated, too.
            foreach (array_diff($expectedFields, array_keys($arrayOfArrays[$i])) as $field) {
                $arrayOfArrays[$i][$field] = null;
            }

            foreach ($arrayOfArrays[$i] as $field => $value) {
                if (empty($itemConstraints[$field])) {
                    throw new \RuntimeException(
                        'No constraints defined for collection key “'.$field.'”'
                    );
                }
                try {
                    $arrayOfArrays[$i][$field] = $this->validator->validate(
                        $itemConstraints[$field],
                        $value,
                        $labels,
                        $object,
                        $arrayOfArrays[$i]
                    );
                } catch (\InvalidArgumentException $e) {
                    $errors[$i][$field] = $e->getMessage();
                }
            }
        }

        if (count($errors)) {
            $msgs = array();
            foreach (array_values($errors) as $fields) {
                foreach (array_values($fields) as $msg) {
                    $msgs[] = $msg;
                }
            }
            throw new InvalidCollectionArgumentException(join("\n", $msgs), $errors);
        }

        return $arrayOfArrays;
    }
}

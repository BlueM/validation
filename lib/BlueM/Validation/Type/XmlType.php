<?php

namespace BlueM\Validation\Type;

use BlueM\Validation\Constraint\Wellformed;
use BlueM\Validation\Type;

/**
 * Defines the Xml validation type, which is basically the same as the Text type
 * plus a check for well-formedness
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class XmlType extends TextType
{
    /**
     * Constructor. Sets the constraints and transformers for the type
     */
    public function __construct()
    {
        parent::__construct();
        $this->addConstraint(new Wellformed());
    }
}

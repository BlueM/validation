<?php

namespace BlueM\Validation\Type;

use BlueM\Validation\Constraint\Scalar;
use BlueM\Validation\Transformer\FloatTransformer;
use BlueM\Validation\Transformer\TrimTransformer;
use BlueM\Validation\Type;
use BlueM\Validation\I18n;

/**
 * Defines the Float validation type
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class FloatType extends Type
{
    /**
     * @param I18n $i18n
     * @param bool $localized
     */
    public function __construct(I18n $i18n, $localized)
    {
        $this->addConstraint(new Scalar());

        $this->addTransformer(new TrimTransformer());

        $this->addTransformer(
            new FloatTransformer(
                $localized ? $i18n->getDecimalSeparator() : '.',
                $localized ? $i18n->getThousandsSeparator() : ''
            )
        );
    }
}

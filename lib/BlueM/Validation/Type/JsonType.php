<?php

namespace BlueM\Validation\Type;

use BlueM\Validation\Constraint\NotEmpty;
use BlueM\Validation\Constraint\Json;
use BlueM\Validation\Transformer\TrimTransformer;
use BlueM\Validation\Type;

/**
 * Defines the Json validation type, which is basically the same as the Text type
 * plus a check for JSON-compliance
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class JsonType extends TextType
{
    /**
     * Constructor. Sets the constraints and transformers for the type
     */
    public function __construct()
    {
    	parent::__construct();
        $this->addTransformer(new TrimTransformer());
        $this->addConstraint(new Json());
    }
}

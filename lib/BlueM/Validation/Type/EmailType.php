<?php

namespace BlueM\Validation\Type;

use BlueM\Validation\Constraint\Maxlength;
use BlueM\Validation\Constraint\Pattern;
use BlueM\Validation\Constraint\Scalar;
use BlueM\Validation\Transformer\TrimTransformer;
use BlueM\Validation\Type;
use BlueM\Validation\ValidationFailure;

/**
 * Defines the Email validation type
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class EmailType extends Type
{
    /**
     * Constructor. Sets the constraints and transformers for the type
     */
    public function __construct()
    {
        $this->addConstraint(new Scalar());

        $this->addTransformer(new TrimTransformer());

        $this->addConstraint(new Maxlength(254));

        $re = '#^[^\s<>äöüÄÖÜß@"]+@(?:[^\s@]+\.[a-z]{2,8}|[\d.]{7,15})$#i';
        $this->addConstraint(new Pattern($re, ValidationFailure::NOT_EMAIL));
    }
}

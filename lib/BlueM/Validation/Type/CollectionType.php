<?php

namespace BlueM\Validation\Type;

use BlueM\Validation\InvalidCollectionArgumentException;
use BlueM\Validation\Type;

/**
 * A type that holds one or more items of same type/with identical constraints
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class CollectionType extends AbstractCollectionType
{
    /**
     * {@inheritDoc}
     */
    public function validate(
        $array,
        $object = null,
        $collectionItem = null,
        array $labels = null
    ) {
        $errors = array();

        $this->checkOuterArray($array);

        for ($i = 0, $ii = count($array); $i < $ii; $i++) {
            try {
                $array[$i] = $this->validator->validate(
                    $this->constraints,
                    $array[$i]
                );
            } catch (\InvalidArgumentException $e) {
                $errors[$i] = $e->getMessage();
            }

            if (count($errors)) {
                throw new InvalidCollectionArgumentException(join("\n", $errors), $errors);
            }
        }

        return $array;
    }
}

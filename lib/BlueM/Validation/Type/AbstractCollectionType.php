<?php

namespace BlueM\Validation\Type;

use BlueM\Validation\Type;
use BlueM\Validation\ValidationFailedException;
use BlueM\Validation\ValidationFailure;
use BlueM\Validation\Validator;

/**
 * Abstract supertype for the collection types
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
abstract class AbstractCollectionType extends Type
{
    /**
     * @var Validator
     */
    protected $validator;

    /**
     * @var array
     */
    protected $constraints;

    /**
     * @param Validator $validator
     * @param array     $constraints
     */
    public function __construct(Validator $validator, array $constraints)
    {
        $this->validator   = $validator;
        $this->constraints = $constraints;
    }

    /**
     * @param $array
     *
     * @throws ValidationFailedException
     */
    protected function checkOuterArray($array)
    {
        if (!empty($this->constraints['mincount']) ||
            !empty($this->constraints['mandatory'])
        ) {
            $mandatory = true;
        } else {
            $mandatory = false;
        }

        if (is_null($array)) {
            if ($mandatory) {
                throw new ValidationFailedException(ValidationFailure::MANDATORY);
            }
        } elseif (is_array($array)) {
            if ($mandatory && !count($array)) {
                throw new ValidationFailedException(ValidationFailure::MANDATORY);
            }

            if (!empty($this->constraints['mincount']) &&
                count($array) < $this->constraints['mincount']
            ) {
                throw new ValidationFailedException(
                    ValidationFailure::TOO_FEW,
                    $this->constraints['mincount']
                );
            }
        } else {
            throw new ValidationFailedException(ValidationFailure::WRONG_TYPE);
        }

        unset($this->constraints['mincount']);
    }
}

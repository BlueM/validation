<?php

namespace BlueM\Validation\Type;

use BlueM\Validation\Constraint\Scalar;
use BlueM\Validation\Transformer\BoolTransformer;
use BlueM\Validation\Type;

/**
 * Defines the Bool validation type
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class BoolType extends Type
{
    /**
     * Constructor. Sets the constraints and transformers for the type
     */
    public function __construct()
    {
        $this->addConstraint(new Scalar());

        $this->addTransformer(new BoolTransformer());
    }
}

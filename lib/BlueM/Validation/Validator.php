<?php

namespace BlueM\Validation;

use BlueM\Validation\Type\ArrayCollection;
use BlueM\Validation\Type\ArrayCollectionType;
use BlueM\Validation\Type\Collection;
use BlueM\Validation\Type\CollectionType;

/**
 * Simple validation service
 *
 * Validation can either be done via annotations on class properties (preferred) or
 * by explicitly specifying constraints/validation rules as argument to a method
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class Validator
{
    /**
     * @var I18n
     */
    protected $i18n;

    /**
     * @var array
     */
    protected $namespaces = array(
        __NAMESPACE__ => array(),
    );

    /**
     * @param I18n $i18n
     */
    public function __construct(I18n $i18n)
    {
        $this->i18n = $i18n;
    }

    /**
     * @param string $ns
     * @param array  $arguments
     *
     * @throws \InvalidArgumentException
     */
    public function addNamespace($ns, array $arguments = array())
    {
        if (!is_string($ns)) {
            throw new \InvalidArgumentException('Argument 1 must be a string');
        }
        if (isset($this->namespaces[$ns])) {
            throw new \InvalidArgumentException('Namespace already registered');
        }
        $this->namespaces[$ns] = $arguments;
    }

    /**
     * Sets the I18n instance to use, in case it has to be changed after instantiation.
     *
     * @param I18n $i18n
     */
    public function setI18n(I18n $i18n)
    {
        $this->i18n = $i18n;
    }

    /**
     * Tests whether the given value complies with the annotation-based constraints for
     * the property with the given name.
     *
     * @param object $object Instance which contains the property
     * @param string $name   Property name
     * @param mixed  $value  The value to be tested
     * @param array  $labels
     *
     * @return mixed
     */
    public function validatePropertyValue(
        $object,
        $name,
        $value,
        array $labels = array()
    ) {
        if (!is_object($object)) {
            throw new \InvalidArgumentException('Argument 1 must be an object');
        }

        $constraints = AnnotationReader::getConstraints(
            new \ReflectionProperty(get_class($object), $name)
        );

        return $this->validate($constraints, $value, $labels, $object, null);
    }

    /**
     * Validates the current value of a property, either by using property annotations or
     * the constraints given as arguments.
     *
     * @param object $object      Instance which contains the property
     * @param string $name        Property name
     * @param array  $constraints If not given, will read from property annotations
     * @param array  $labels
     *
     * @return mixed
     */
    public function validateProperty(
        $object,
        $name,
        array $constraints = null,
        array $labels = array()
    ) {
        if (!is_object($object)) {
            throw new \InvalidArgumentException('Argument 1 must be an object');
        }

        $rp = new \ReflectionProperty(get_class($object), $name);

        if (!$constraints) {
            $constraints = AnnotationReader::getConstraints($rp);
        }

        return $this->validate($constraints, $rp->getValue($object), $labels, $object);
    }

    /**
     * Validates values of all annotated properties of the given object.
     *
     * @param object     $object
     * @param array|null $constraints
     * @param array      $labels
     *
     * @throws \InvalidArgumentException
     * @return array
     */
    public function validateObject(
        $object,
        array $constraints = null,
        array $labels = array()
    ) {
        if (!is_object($object)) {
            throw new \InvalidArgumentException('Argument 1 must be an object');
        }

        if ($constraints) {
            return $this->validateObjectUsingGivenConstraints(
                $object,
                $constraints,
                $labels
            );
        }

        return $this->validateObjectUsingAnnotations($object, $labels);
    }

    /**
     * Validates values of all annotated properties of the given object.
     *
     * @param object $object
     * @param array  $labels
     *
     * @return array
     */
    protected function validateObjectUsingAnnotations($object, array $labels)
    {
        $class      = new \ReflectionClass($object);
        $properties = $this->getCheckableProperties($class);
        $errors     = array();

        foreach ($properties as $name => $data) {
            $property    = $data['property'];
            $constraints = $data['constraints'];

            try {
                $this->validate(
                    $constraints,
                    $property->getValue($object),
                    $labels,
                    $object
                );
            } catch (InvalidCollectionArgumentException $e) {
                $errors[$name] = $e->getErrors();
            } catch (\InvalidArgumentException $e) {
                $errors[$name] = $e->getMessage();
            }
        }

        return $errors;
    }

    /**
     * Validates values of all annotated properties of the given object.
     *
     * @param object $object
     * @param array  $constraints
     * @param array  $labels
     *
     * @return array
     */
    protected function validateObjectUsingGivenConstraints(
        $object,
        array $constraints,
        array $labels
    ) {
        $errors = array();

        foreach ($constraints as $name => $propertyConstraints) {
            $property = new \ReflectionProperty($object, $name);
            $property->setAccessible(true);

            try {
                $this->validate(
                    $propertyConstraints,
                    $property->getValue($object),
                    $labels,
                    $object,
                    null
                );
            } catch (InvalidCollectionArgumentException $e) {
                $errors[$name] = $e->getErrors();
            } catch (\InvalidArgumentException $e) {
                $errors[$name] = $e->getMessage();
            }
        }

        return $errors;
    }

    /**
     * Performs direct validation of a value using the given constraints.
     *
     * If validation succeeds, returns the value, possibly (depending on the type)
     * converted to a generic, non-localized representation. If validation fails, throws
     * an InvalidArgumentException.
     *
     * @param array $constraints
     * @param mixed $value
     * @param array $labels
     *
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     * @return mixed
     */
    public function validate(
        array $constraints,
        $value,
        array $labels = array()
    ) {
        if (empty($constraints['type'])) {
            throw new \RuntimeException('The type must be defined');
        }

        $collectionItem = $object = null;
        if (func_num_args() > 3) {
            $object = func_get_arg(3);
            if (func_num_args() > 4) {
                $collectionItem = func_get_arg(4);
            }
        }

        $constraints['type'] = strtolower($constraints['type']);

        if (empty($constraints['mandatory'])) {
            unset($constraints['mandatory']);
        }

        if (isset($constraints['notlocalized'])) {
            $localized = false;
            unset($constraints['notlocalized']);
        } else {
            $localized = true;
        }

        unset($constraints['label']);

        if ($constraints['type'] == 'collection' ||
            $constraints['type'] == 'arraycollection'
        ) {
            throw new \RuntimeException(
                'Do not use “collection” (instead: “string[]”, “email[]”, ...) or '.
                '“arraycollection” (instead: “array[]”) as the type'
            );
        }

        /** @var $typeInstance \BlueM\Validation\Type */
        if ('[]' == substr($constraints['type'], -2)) {
            // Collection
            $constraints['type'] = substr($constraints['type'], 0, -2);
            if ('array' === $constraints['type']) {
                $typeInstance = new ArrayCollectionType($this, $constraints);
                if (empty($constraints['mandatory'])) {
                    $constraints = array();
                } else {
                    $constraints = array('mandatory' => true);
                }
            } else {
                $typeInstance = new CollectionType($this, $constraints);
            }
            unset($constraints['mincount']);
        } else {
            $typeInstance = $this->getTypeInstance($constraints, $localized);
        }

        unset($constraints['type']);

        foreach ($constraints as $type => $data) {
            $class = __NAMESPACE__.'\\Constraint\\'.ucfirst(strtolower($type));
            if (!class_exists($class)) {
                throw new \RuntimeException("Unsupported constraint “".$type."”");
            }
            $typeInstance->addConstraint(new $class($data));
        }

        try {
            $value = $typeInstance->validate(
                $value,
                $object,
                $collectionItem,
                $labels
            );
        } catch (ValidationFailedException $e) {
            $msg = $typeInstance->getExceptionMessage($this->i18n, $e, $labels);
            throw new \InvalidArgumentException($msg, $e->getCode(), $e);
        }

        return $value;
    }

    /**
     * Returns the value of a specific validation annotation
     *
     * @param string $class    FQ class name
     * @param string $property Property name
     * @param string $name     Annotation name, without "validation-" prefix
     *
     * @throws \InvalidArgumentException
     * @return array|mixed
     */
    public static function getConstraint($class, $property, $name)
    {
        if (!class_exists($class)) {
            throw new \InvalidArgumentException(
                'Could not find a class named “'.$class.'”'
            );
        }

        return AnnotationReader::getConstraint(
            new \ReflectionProperty($class, $property),
            $name
        );
    }

    /**
     * @param \ReflectionClass $class
     *
     * @return array
     */
    protected function getCheckableProperties(\ReflectionClass $class)
    {
        $properties = array();

        foreach ($class->getProperties() as $property) {
            $constraints = AnnotationReader::getConstraints($property);
            if (empty($constraints)) {
                // Obviously not a property to be validated
                continue;
            }
            $properties[$property->getName()] = array(
                'property'    => $property,
                'constraints' => $constraints,
            );
            $property->setAccessible(true);
        }

        return $properties;
    }

    /**
     * @param array $constraints
     * @param bool  $localized
     *
     * @return Type
     */
    protected function getTypeInstance(array $constraints, $localized)
    {
        foreach (array_keys($this->namespaces) as $namespace) {
            $class = "$namespace\\Type\\".ucfirst($constraints['type']).'Type';

            if (class_exists($class)) {
                if (count($this->namespaces[$namespace])) {
                    // Additional arguments
                    $args = $this->namespaces[$namespace];
                    array_unshift($args, $this->i18n, $localized);
                    $class = new \ReflectionClass($class);

                    return $class->newInstanceArgs($args);
                }

                return new $class($this->i18n, $localized);
            }
        }

        throw new \RuntimeException('Unknown type “'.$constraints['type'].'”');
    }
}

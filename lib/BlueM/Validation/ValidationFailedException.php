<?php

namespace BlueM\Validation;

/**
 * A ValidationFailedException is thrown either when a constraint is violated or
 * when a transformer encounters unexpected/unparseable input.
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
class ValidationFailedException extends \InvalidArgumentException
{
    /**
     * @var mixed
     */
    protected $placeholderValue;

    /**
     * @param int    $code             One of the Validator constants
     * @param string $placeholderValue String to insert as placeholder value
     */
    public function __construct($code, $placeholderValue = '')
    {
        parent::__construct('Validation failed', $code);
        $this->placeholderValue = $placeholderValue;
    }

    /**
     * Returns the placeholder value
     *
     * @return string
     */
    public function getPlaceholderValue()
    {
        return $this->placeholderValue;
    }
}

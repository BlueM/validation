<?php

namespace BlueM\Validation;

/**
 * Abstract base class for the i18n classes
 *
 * @author  Carsten Bluem <carsten@bluem.net>
 * @license http://www.opensource.org/licenses/bsd-license.php BSD 2-Clause License
 * @link    https://bitbucket.org/BlueM/validation
 */
abstract class I18n
{
    /**
     * @var array
     */
    private $strings = array();

    /**
     * @var array
     */
    protected $customStrings = array();

    /**
     * @var string
     */
    protected $decimalSeparator = '.';

    /**
     * @var string
     */
    protected $thousandsSeparator = ',';

    /**
     * @var string
     */
    protected $dateFormat = 'm/d/Y';

    /**
     * Returns whether or not "pm"/"am" suffixes be used to indicate the period
     *
     * @return bool
     */
    abstract public function acceptsPeriod();

    /**
     * @return array
     */
    abstract protected function getStrings();

    /**
     * @param array $strings
     */
    public function __construct(array $strings = array())
    {
        if (count($strings)) {
            $this->customStrings = $strings;
        }
    }

    /**
     * Returns the string to be used as thousands separator
     *
     * @return string
     */
    public function getThousandsSeparator()
    {
        return $this->thousandsSeparator;
    }

    /**
     * Returns the string to be used as decimal separator
     *
     * @return string
     */
    public function getDecimalSeparator()
    {
        return $this->decimalSeparator;
    }

    /**
     * @return string
     */
    public function getDateFormat()
    {
        return $this->dateFormat;
    }

    /**
     * Returns a localized for the given string identifier
     *
     * @param string $key              Key of the string / phrase to return.
     * @param string $placeholderValue Value to use for a placeholder
     *
     * @return string The string / phrase.
     */
    public function getString($key, $placeholderValue = null)
    {
        if (array() === $this->strings) {
            $this->strings = $this->customStrings + $this->getStrings();
        }

        if (empty($this->strings[$key])) {
            return $key;
        }

        return sprintf($this->strings[$key], $placeholderValue);
    }
}

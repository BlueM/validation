<?php

use BlueM\Validation\I18n\De;
use BlueM\Validation\I18n\En;
use BlueM\Validation\Validator;

require_once __DIR__ . '/bootstrap.php';

/**
 * Class DummyDomainObject
 */
class DummyDomainObject
{
    /**
     * @validation-type string
     * @validation-maxlength 400
     */
    public $string;

    /**
     * @validation-type int
     */
    public $localizedInt;

    /**
     * @validation-type int
     * @validation-notlocalized
     */
    public $nonLocalizedInt;

    /**
     * @validation-type float
     */
    public $localizedFloat;

    /**
     * @validation-type float
     * @validation-notlocalized
     */
    public $nonLocalizedFloat;

    /**
     * @validation-type date
     */
    public $localizedDate;

    /**
     * @validation-type date
     * @validation-notlocalized
     */
    public $nonLocalizedDate;

    /**
     * @validation-type string
     */
    public $conditional1;

    /**
     * @validation-type string
     * @validation-mandatory IF $conditional1
     */
    public $conditional2;

    /**
     * @validation-type string
     * @validation-mandatory IF NOT $conditional1
     */
    public $conditional3;

    /**
     * @validation-type string[]
     * @validation-mandatory
     */
    public $stringCollection1;

    /**
     * @validation-type string[]
     * @validation-mincount 1
     */
    public $stringCollection2;

    /**
     * @validation-type string[]
     * @validation-mincount 3
     */
    public $stringCollection3;

    /**
     * @validation-type array[]
     * @validation-mandatory
     * @validation-type:title string
     * @validation-mandatory:title
     * @validation-minlength:title 10
     * @validation-maxlength:title 50
     * @validation-type:startdate date
     * @validation-mandatory:startdate
     */
    public $arrayCollection1 = array();

    /**
     * @validation-type array[]
     * @validation-mincount 3
     * @validation-type:name string
     */
    public $arrayCollection2 = array();

    /**
     * @validation-type array[]
     */
    public $arrayCollection3 = array();

    /**
     * @validation-type xml
     */
    public $xml;

    /**
     * @validation-type email
     */
    public $email;

    /**
     * @validation-type float
     */
    public $float1;

    /**
     * @validation-type float
     * @validation-minimum $float1
     */
    public $float2;
}

/**
 * Integration tests for BlueM\Validation
 * @codeCoverageIgnore
 */
class IntegrationTestUsingAnnotations extends \PHPUnit_Framework_TestCase
{

    /**
     * This test makes sure that DummyDomainObject::$string may be set to a string
     * with more than 250 characters, although by default, Type/String sets a maximum
     * of 250 characters.
     *
     * @test
     */
    public function aConstraintProvidedByATypeCanBeOverriden()
    {
        $i18n = new En();
        $validator = new Validator($i18n);
        $validator->validatePropertyValue(
            new DummyDomainObject(),
            'string',
            str_repeat('x', 350)
        );
    }

    /**
     * Makes sure that an integer that doe not have a @validation-notlocalized annotation
     * may be given with or thousands separator.
     *
     * @test
     */
    public function aLocalizedIntIsAcceptedWhenALocalizedValuesIsAllowed()
    {
        $i18n = new En();
        $validator = new Validator($i18n);

        $actual = $validator->validatePropertyValue(
            new DummyDomainObject(),
            'localizedInt',
            '1,234'
        );
        $this->assertSame(1234, $actual);

        $actual = $validator->validatePropertyValue(
            new DummyDomainObject(),
            'localizedInt',
            1234
        );
        $this->assertSame(1234, $actual);

    }

    /**
     * Makes sure that an integer that has @validation-notlocalized annotation may
     * not be given with thousands separator.
     *
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionCode 16
     */
    public function aLocalizedIntIsNotAcceptedWhenALocalizedValuesIsNotAllowed()
    {
        $i18n = new En();
        $validator = new Validator($i18n);
        $validator->validatePropertyValue(
            new DummyDomainObject(),
            'nonLocalizedInt',
            '1,234'
        );
    }

    /**
     * Makes sure that an float that doe not have a @validation-notlocalized annotation
     * may be given with or without localized decimal and/or thousands separator.
     *
     * @test
     */
    public function aLocalizedFloatIsAcceptedWhenALocalizedValueIsAllowed()
    {
        $i18n = new En();
        $validator = new Validator($i18n);

        $actual = $validator->validatePropertyValue(
            new DummyDomainObject(),
            'localizedFloat',
            '1,234.56'
        );
        $this->assertSame(1234.56, $actual);

        $actual = $validator->validatePropertyValue(
            new DummyDomainObject(),
            'localizedFloat',
            1234.56
        );
        $this->assertSame(1234.56, $actual);

    }

    /**
     * Makes sure that an float that has @validation-notlocalized annotation may
     * not be given with localized decimal separator.
     *
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionCode 7
     */
    public function aLocalizedFloatIsNotAcceptedWhenALocalizedValueIsNotAllowed()
    {
        $i18n = new De();
        $validator = new Validator($i18n);
        $validator->validatePropertyValue(
            new DummyDomainObject(),
            'nonLocalizedFloat',
            '1,234'
        );
    }

    /**
     * Makes sure that a date that does not have a @validation-notlocalized annotation
     * may be given as Y-m-d or in localized format
     *
     * @test
     */
    public function aLocalizedDateIsAcceptedWhenALocalizedValueIsAllowed()
    {
        $i18n = new En();
        $validator = new Validator($i18n);

        $actual = $validator->validatePropertyValue(
            new DummyDomainObject(),
            'localizedDate',
            '12/24/2012'
        );
        $this->assertSame('2012-12-24', $actual);

        $actual = $validator->validatePropertyValue(
            new DummyDomainObject(),
            'localizedDate',
            '2012-12-24'
        );
        $this->assertSame('2012-12-24', $actual);

    }

    /**
     * Makes sure that a date that has @validation-notlocalized annotation
     * is not accepted in localized format
     *
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionCode 13
     */
    public function aLocalizedDateIsNotAcceptedWhenALocalizedValueIsNotAllowed()
    {
        $i18n = new En();
        $validator = new Validator($i18n);
        $validator->validatePropertyValue(
            new DummyDomainObject(),
            'nonLocalizedDate',
            '05/20/2011'
        );
    }

    /**
     * Makes sure that a positive conditional constraint works
     *
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionCode 9
     */
    public function aPositiveConditionalConstraintWorks()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();
        $dummy->conditional1 = 'Non-empty';

        $validator = new Validator($i18n);
        $validator->validatePropertyValue($dummy, 'conditional2', '');
    }

    /**
     * Makes sure that a positive conditional constraint works
     *
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionCode 10
     */
    public function aNegativeConditionalConstraintWorks()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();
        $dummy->conditional1 = '';

        $validator = new Validator($i18n);
        $validator->validatePropertyValue($dummy, 'conditional3', '');
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Invalid data type
     */
    public function validatingACollectionFailsIfAScalarIsGiven()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $validator = new Validator($i18n);
        $validator->validatePropertyValue($dummy, 'stringCollection1', 'Should throw an exception');
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage mandatory
     */
    public function aCollectionMustNotBeEmptyIfDeclaredAsMandatory()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $validator = new Validator($i18n);
        $validator->validatePropertyValue(
            $dummy,
            'stringCollection1',
            []
        );
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage mandatory
     */
    public function aCollectionMustNotBeNullIfDeclaredAsMandatory()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $validator = new Validator($i18n);
        $validator->validatePropertyValue(
            $dummy,
            'stringCollection1',
            null
        );
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage value is mandatory.
     */
    public function aCollectionMustNotBeEmptyIfTheMininmumCountIsSetTo1()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $validator = new Validator($i18n);
        $validator->validatePropertyValue(
            $dummy,
            'stringCollection2',
            []
        );
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage At least 3 values
     */
    public function aCollectionMustNotContainFewerItemsThanTheMincountValue()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $validator = new Validator($i18n);
        $validator->validatePropertyValue(
            $dummy,
            'stringCollection3',
            ['A', 'B']
        );
    }

    /**
     * @test
     */
    public function aCollectionValuePassesValidationIfItIsValid()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $value = ['Foobar'];

        $validator = new Validator($i18n);
        $actual    = $validator->validatePropertyValue(
            $dummy,
            'stringCollection1',
            $value
        );

        $this->assertSame($actual, $value);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Invalid data type
     */
    public function validatingAnArrayCollectionFailsIfAScalarIsGiven()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $validator = new Validator($i18n);
        $validator->validatePropertyValue(
            $dummy,
            'arrayCollection1',
            'Should throw an exception'
        );
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage mandatory
     */
    public function anArrayCollectionMustNotBeEmptyIfDeclaredAsMandatory()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $validator = new Validator($i18n);
        $validator->validatePropertyValue(
            $dummy,
            'arrayCollection1',
            []
        );
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage mandatory
     */
    public function anArrayCollectionMustNotBeNullIfDeclaredAsMandatory()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $validator = new Validator($i18n);
        $validator->validatePropertyValue(
            $dummy,
            'arrayCollection1',
            null
        );
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage value is mandatory.
     */
    public function anArrayCollectionMustNotBeEmptyIfTheMininmumCountIsSetTo1()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $validator = new Validator($i18n);
        $validator->validatePropertyValue(
            $dummy,
            'arrayCollection1',
            array()
        );
    }

    /**
     * @test
     */
    public function anArrayCollectionMayBeEmptyIfItIsNotMandatoryAndDoesNotHaveAMincount()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $validator = new Validator($i18n);
        $actual = $validator->validatePropertyValue(
            $dummy,
            'arrayCollection3',
            array()
        );

        $this->assertSame(array(), $actual);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage At least 3 values
     */
    public function anArrayCollectionMustNotContainFewerItemsThanTheMincountValue()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $validator = new Validator($i18n);
        $validator->validatePropertyValue(
            $dummy,
            'arrayCollection2',
            array(
                array('A'),
                array('B'),
            )
        );
    }

    /**
     * @test
     * @expectedException BlueM\Validation\InvalidCollectionArgumentException
     * @expectedExceptionMessage less than 10 characters
     */
    public function anArrayCollectionFailsValidationIfItContainsAnInvalidValue()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $value = array(
            array(
                'title'     => 'Too short',
                'startdate' => '2014-06-30',
            ),
        );

        $validator = new Validator($i18n);
        $validator->validatePropertyValue(
            $dummy,
            'arrayCollection1',
            $value
        );
    }

    /**
     * @test
     * @expectedException BlueM\Validation\InvalidCollectionArgumentException
     * @expectedExceptionMessage value is mandatory
     */
    public function anArrayCollectionFailsValidationIfAMandatoryValueIsMissing()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $value = array(
            array(
                'startdate' => '2014-06-30',
            ),
        );

        $validator = new Validator($i18n);
        $validator->validatePropertyValue(
            $dummy,
            'arrayCollection1',
            $value
        );
    }

    /**
     * @test
     * @expectedException RuntimeException
     * @expectedExceptionMessage No constraints defined for collection key “nosuchthing”
     */
    public function anArrayCollectionFailsValidationIfNoConstraintsAreDefinedForAKey()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $value = array(
            array(
                'title'       => 'A',
                'startdate'   => 'B',
                'nosuchthing' => 'C',
            ),
        );

        $validator = new Validator($i18n);
        $validator->validatePropertyValue(
            $dummy,
            'arrayCollection1',
            $value
        );
    }

    /**
     * @test
     */
    public function anArrayCollectionValuePassesValidationIfItIsValid()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $value = array(
            array(
                'title'     => 'Hello world',
                'startdate' => '2014-06-30',
            ),
        );

        $validator = new Validator($i18n);
        $actual    = $validator->validatePropertyValue(
            $dummy,
            'arrayCollection1',
            $value
        );

        $this->assertSame($actual, $value);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage This is not well-formed XML
     */
    public function anXmlStringFailsValidationIfIsIsNotWellformed()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $validator = new Validator($i18n);
        $validator->validatePropertyValue(
            $dummy,
            'xml',
            '<a></B>'
        );
    }

    /**
     * @test
     */
    public function anXmlStringPassesValidationIfItIsValid()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $xml = '<root><a>Hello world</a><foo bar="1" /></root>';

        $validator = new Validator($i18n);
        $actual    = $validator->validatePropertyValue(
            $dummy,
            'xml',
            $xml
        );

        $this->assertSame($actual, $xml);
    }

    /**
     * @test
     */
    public function anEmailAddressPassesValidationIfItIsValid()
    {
        $i18n = new En();

        $dummy = new DummyDomainObject();

        $validator = new Validator($i18n);
        $actual    = $validator->validatePropertyValue(
            $dummy,
            'email',
            'foo@example.com '
        );

        $this->assertSame($actual, 'foo@example.com');
    }

    /**
     * @test
     */
    public function validatingAnObjectSucceedsIfAllPropertiesAreValid()
    {
        $i18n = new En();

        $subject = new DummyDomainObject();

        $subject->conditional1 = 'String 1';
        $subject->conditional2 = 'String 2';
        $subject->stringCollection1 = array('String1', 'String2');
        $subject->stringCollection2 = array('String1', 'String2');
        $subject->stringCollection3 = array('String1', 'String2', 'String3');

        $subject->arrayCollection1 = array(
            array(
                'title'     => 'Foobar 4711',
                'startdate' => '2014-08-15',
            )
        );

        $subject->arrayCollection2 = array(
            array('name' => 'A'),
            array('name' => 'B'),
            array('name' => 'C'),
        );

        $validator = new Validator($i18n);

        $actual = $validator->validateObject($subject);
        $this->assertInternalType('array', $actual);
        $this->assertEquals(array(), $actual);
    }

    /**
     * @test
     */
    public function validatingAnObjecReturnsAllErrors()
    {
        $i18n = new En();

        $subject = new DummyDomainObject();

        $subject->float1            = 123.45;
        $subject->float2            = 119;
        $subject->conditional1      = 'A string';
        $subject->stringCollection1 = array('String1', 'String2');
        $subject->stringCollection2 = array('String1', 'String2');
        $subject->stringCollection3 = array('String1', 'String2', 'String3');

        $subject->arrayCollection1 = array(
            array(
                'title'     => 'Foobar 4711',
                'startdate' => '2014-08-15',
            )
        );

        $subject->arrayCollection2 = array(
            array(
                'name' => 'Foo',
            )
        );

        $validator = new Validator($i18n);

        $actual = $validator->validateObject($subject, null, array('$conditional1' => 'Conditional 1'));
        $this->assertInternalType('array', $actual);
        $this->assertEquals(
            array(
                'arrayCollection2' => 'At least 3 values are required.',
                'float2' => 'The value must not be smaller than 123.45.',
                'conditional2' => 'This value is mandatory, if field Conditional 1 is not empty.',
            ),
            $actual
        );
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage must not be smaller than 123.45
     */
    public function validatingAPropertyAlsoValidatesDependencyConstraints()
    {
        $i18n = new En();

        $subject         = new \DummyDomainObject;
        $subject->float1 = 123.45;

        $validator = new Validator($i18n);

        $validator->validatePropertyValue(
            $subject,
            'float2',
            120
        );
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage This value is mandatory, if field Conditional 1 is not empty
     */
    public function validatingAPropertyUsesALabelCallback()
    {
        $i18n = new En();

        $subject               = new \DummyDomainObject;
        $subject->conditional1 = 'A string';

        $validator = new Validator($i18n);

        $validator->validatePropertyValue(
            $subject,
            'conditional2',
            '',
            array('$conditional1' => 'Conditional 1')
        );
    }
}

<?php

namespace BlueM\Validation\Transformer;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Transformer\BoolTransformer
 *
 * @covers BlueM\Validation\Transformer\BoolTransformer
 */
class BoolTransformerTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @test
	 */
	public function aNonEmptyStringIsTransformedToTrue()
	{
        $subject = new BoolTransformer();
        $this->assertTrue($subject->transform('a'));
	}

	/**
	 * @test
	 */
	public function aNonZeroNumberIsTransformedToTrue()
	{
        $subject = new BoolTransformer();
        $this->assertTrue($subject->transform(0.01));
	}

	/**
	 * @test
	 */
	public function aNotEmptyArrayIsTransformedToTrue()
	{
        $subject = new BoolTransformer();
        $this->assertTrue($subject->transform(array(0)));
	}

	/**
	 * @test
	 */
	public function anEmptyStringIsTransformedToFalse()
	{
        $subject = new BoolTransformer();
        $this->assertFalse($subject->transform(''));
	}

	/**
	 * @test
	 */
	public function zeroIsTransformedToFalse()
	{
        $subject = new BoolTransformer();
        $this->assertFalse($subject->transform(0));
	}

	/**
	 * @test
	 */
	public function anEmptyArrayIsTransformedToFalse()
	{
        $subject = new BoolTransformer();
        $this->assertFalse($subject->transform(array()));
	}
}

<?php

namespace BlueM\Validation\Transformer;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Transformer\MonthTransformer
 *
 * @covers BlueM\Validation\Transformer\MonthTransformer
 */
class MonthTransformerTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @test
	 */
	public function nullIsReturnedAsNull()
	{
        $subject = new MonthTransformer();
        $this->assertNull($subject->transform(null));
	}

	/**
	 * @test
	 */
	public function anEmptyStringIsReturnedAsNull()
	{
        $subject = new MonthTransformer();
        $this->assertNull($subject->transform(''));
	}

	/**
	 * @test
	 */
	public function aMonthTransformerInYyyymmFormatWithHyphenAsSeparatorIsRegardedAsValid()
	{
        $subject = new MonthTransformer();
        $this->assertSame('2014-02', $subject->transform('2014-02'));
	}

	/**
	 * @test
	 */
	public function aMonthTransformerInYyyymmFormatWithSpaceAsSeparatorIsRegardedAsValid()
	{
        $subject = new MonthTransformer();
        $this->assertSame('2014-02', $subject->transform('2014 02'));
	}

	/**
	 * @test
	 */
	public function aMonthTransformerInYyyymmFormatWithDotAsSeparatorIsRegardedAsValid()
	{
        $subject = new MonthTransformer();
        $this->assertSame('2012-10', $subject->transform('2012.10'));
	}

	/**
	 * @test
	 */
	public function aMonthTransformerInYyyymmFormatWithSlashAsSeparatorIsRegardedAsValid()
	{
        $subject = new MonthTransformer();
        $this->assertSame('2014-02', $subject->transform('2014/02'));
	}

	/**
	 * @test
	 */
	public function aMonthTransformerInMmyyyyFormatWithHyphenAsSeparatorIsRegardedAsValid()
	{
        $subject = new MonthTransformer();
        $this->assertSame('2013-11', $subject->transform('11-2013'));
	}

	/**
	 * @test
	 */
	public function aMonthTransformerInMmyyyyFormatWithSpaceAsSeparatorIsRegardedAsValid()
	{
        $subject = new MonthTransformer();
        $this->assertSame('2014-02', $subject->transform('02 2014'));
	}

	/**
	 * @test
	 */
	public function aMonthTransformerInMmyyyyFormatWithDotAsSeparatorIsRegardedAsValid()
	{
        $subject = new MonthTransformer();
        $this->assertSame('2014-02', $subject->transform('02.2014'));
	}

	/**
	 * @test
	 */
	public function aMonthTransformerInMmyyyyFormatWithSlashAsSeparatorIsRegardedAsValid()
	{
        $subject = new MonthTransformer();
        $this->assertSame('2014-02', $subject->transform('02/2014'));
	}

	/**
	 * @test
	 */
	public function aMonthTransformerInMyyyyFormatIsRegardedAsValid()
	{
        $subject = new MonthTransformer();
        $this->assertSame('2014-02', $subject->transform('2/2014'));
	}

	/**
	 * @test
	 */
	public function aMonthTransformerInYyyymFormatIsRegardedAsValid()
	{
        $subject = new MonthTransformer();
        $this->assertSame('2014-02', $subject->transform('2014-2'));
	}

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 24
     */
    public function aMonthTransformerSmallerThan1IsRegardedAsInvalid()
    {
        $subject = new MonthTransformer();
        $subject->transform('0/2009');
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 24
     */
    public function aMonthTransformerGreaterThan12IsRegardedAsInvalid()
    {
        $subject = new MonthTransformer();
        $subject->transform('13/2009');
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 24
     */
    public function aYearSmallerThan1900IsRegardedAsInvalid()
    {
        $subject = new MonthTransformer();
        $subject->transform('08/1899');
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 24
     */
    public function aYearGreaterThan2100IsRegardedAsInvalid()
    {
        $subject = new MonthTransformer();
        $subject->transform('08/2101');
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 24
     */
    public function aMonthTransformerWithATwoDigitYearIsRegardedAsInvalid()
    {
        $subject = new MonthTransformer();
        $subject->transform('3/09');
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 24
     */
    public function aLiteralMonthTransformerNameIsRegardedAsInvalid()
    {
        $subject = new MonthTransformer();
        $subject->transform('May 2007');
    }
}

<?php

namespace BlueM\Validation\Transformer;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Transformer\FloatTransformer
 *
 * @covers BlueM\Validation\Transformer\FloatTransformer
 */
class FloatTransformerTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage decimal separator may not be empty
	 */
	public function anEmptyDecimalSeparatorIsNotAccepted()
	{
        $subject = new FloatTransformer('', '.');
        $this->assertSame(12345.0, $subject->transform('12345'));
	}

	/**
	 * @test
	 */
	public function aPositiveIntegerIsRegardedAsValid()
	{
        $subject = new FloatTransformer('.', ',');
        $this->assertSame(12345.0, $subject->transform('12345'));
	}

	/**
	 * @test
	 */
	public function aNegativeIntegerIsRegardedAsValid()
	{
        $subject = new FloatTransformer('.', ',');
        $this->assertSame(-12345.0, $subject->transform('-12345'));
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 7
	 */
	public function aMinusWithoutDigitsIsRegardedAsInvalid()
	{
        $subject = new FloatTransformer('.', ',');
        $subject->transform('-');
	}

    /**
     * @test
     */
    public function aPositiveFloatTransformerIsRegardedAsValid()
    {
        $subject = new FloatTransformer('.', ',');;
        $this->assertSame(123.45, $subject->transform('123.45'));
    }

    /**
     * @test
     */
    public function aNegativeFloatTransformerIsRegardedAsValid()
    {
        $subject = new FloatTransformer('.', ',');;
        $this->assertSame(-123.45, $subject->transform('-123.45'));
    }

    /**
     * @test
     */
    public function aPositiveFloatTransformerWithThousandsSeparatorIsRegardedAsValid()
    {
        $subject = new FloatTransformer('.', ',');
        $this->assertSame(1234567.89, $subject->transform('1,234,567.89'));
    }

    /**
     * @test
     */
    public function aNegativeFloatTransformerWithThousandsSeparatorIsRegardedAsValid()
    {
        $subject = new FloatTransformer('.', ',');
        $this->assertSame(-123456.789, $subject->transform('-123,456.789'));
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 7
     */
    public function aNumberContainingACharacterIsRegardedAsInvalid()
    {
        $subject = new FloatTransformer('.', ',');
        $subject->transform('123a5');
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 7
     */
    public function aNumberWithATrailingCharacterIsRegardedAsInvalid()
    {
        $subject = new FloatTransformer('.', ',');
        $subject->transform('123a');
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 7
     */
    public function aNumberWithACharacterAfterTheDecimalSeparatorIsRegardedAsInvalid()
    {
        $subject = new FloatTransformer('.', ',');
        $subject->transform('123.a');
    }

    /**
     * @test
     */
    public function aFloatTransformerInGermanLocalizationIsRegardedAsValid()
    {
        $subject = new FloatTransformer(',', '.');
        $this->assertSame(-123456.789, $subject->transform('-123.456,789'));
    }

    /**
     * @test
     */
    public function aFloatTransformerIsRegardedAsValidWhenNoThousandsSeparatorIsDefined()
    {
        $subject = new FloatTransformer('.', '');
        $this->assertSame(-123456.789, $subject->transform('-123456.789'));
    }

    /**
     * @test
     */
    public function zeroIsRegardedAsValid()
    {
        $subject = new FloatTransformer('.', '');
        $this->assertSame(0.0, $subject->transform(0));
    }

    /**
     * @test
     */
    public function passingNullReturnsNull()
    {
        $subject = new FloatTransformer('.', '');
        $this->assertSame(null, $subject->transform(null));
    }

    /**
     * @test
     */
    public function passingAnEmptyStringReturnsNull()
    {
        $subject = new FloatTransformer('.', '');
        $this->assertSame(null, $subject->transform(''));
    }
}

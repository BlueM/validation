<?php

namespace BlueM\Validation\Transformer;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Transformer\AddHTTPSchemeTransformer
 *
 * @covers BlueM\Validation\Transformer\AddHTTPSchemeTransformer
 */
class AddHTTPSchemeTransformerTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @test
	 */
	public function nullIsReturnedUnmodified()
	{
        $subject = new AddHTTPSchemeTransformer();
        $this->assertSame(null, $subject->transform(null));
	}

	/**
	 * @test
	 */
	public function anEmptyStringIsReturnedUnmodified()
	{
        $subject = new AddHTTPSchemeTransformer();
        $this->assertSame('', $subject->transform(''));
	}

	/**
	 * @test
	 */
	public function aStringIncludingSchemeIsReturnedUnmodified()
	{
        $subject = new AddHTTPSchemeTransformer();
        $this->assertSame('http://www.example.com', $subject->transform('http://www.example.com'));
	}

	/**
	 * @test
	 */
	public function aStringWithoutSchemeAndWWWIsReturnedUnmodified()
	{
        $subject = new AddHTTPSchemeTransformer();
        $this->assertSame('example.com', $subject->transform('example.com'));
	}

	/**
	 * @test
	 */
	public function aStringWithoutSchemeThatStartsWithWwwIsPrefixedWithHttpScheme()
	{
        $subject = new AddHTTPSchemeTransformer();
        $this->assertSame('http://www.example.com', $subject->transform('www.example.com'));
    }

	/**
	 * @test
	 */
	public function aStringWithoutSchemeThatStartsWithUppercaseWwwIsPrefixedWithHttpScheme()
	{
        $subject = new AddHTTPSchemeTransformer();
        $this->assertSame('http://WWW.example.com', $subject->transform('WWW.example.com'));
    }
}

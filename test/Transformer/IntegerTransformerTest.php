<?php

namespace BlueM\Validation\Transformer;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Transformer\IntegerTransformer
 *
 * @covers BlueM\Validation\Transformer\IntegerTransformer
 */
class IntegerTransformerTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @test
	 */
	public function aPositiveIntegerTransformerIsRegardedAsValid()
	{
        $subject = new IntegerTransformer('.', ',');
        $this->assertSame(12345, $subject->transform('12345'));
	}

	/**
	 * @test
	 */
	public function aNegativeIntegerTransformerIsRegardedAsValid()
	{
        $subject = new IntegerTransformer('.', ',');
        $this->assertSame(-12345, $subject->transform('-12345'));
	}

    /**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 16
	 */
	public function aMinusWithoutDigitsIsRegardedAsInvalid()
	{
        $subject = new IntegerTransformer('.', ',');
        $subject->transform('-');
	}

    /**
     * @test
     */
    public function aPositiveIntegerTransformerWithThousandsSeparatorIsRegardedAsValid()
    {
        $subject = new IntegerTransformer('.', ',');
        $this->assertSame(1234567, $subject->transform('1,234,567'));
    }

    /**
     * @test
     */
    public function aNegativeIntegerTransformerWithThousandsSeparatorIsRegardedAsValid()
    {
        $subject = new IntegerTransformer('.', ',');
        $this->assertSame(-123456, $subject->transform('-123,456'));
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 16
     */
    public function aIntegerTransformerWithATrailingCharacterIsRegardedAsInvalid()
    {
        $subject = new IntegerTransformer('.', ',');
        $subject->transform('123a5');
    }

    /**
     * @test
     */
    public function anIntegerTransformerInGermanLocalizationIsRegardedAsValid()
    {
        $subject = new IntegerTransformer(',', '.');
        $this->assertSame(-123456, $subject->transform('-123.456'));
    }

    /**
     * @test
     */
    public function zeroIsRegardedAsValid()
    {
        $subject = new IntegerTransformer('.', '');
        $this->assertSame(0, $subject->transform('0'));
    }

    /**
     * @test
     */
    public function passingNullReturnsNull()
    {
        $subject = new IntegerTransformer('.', '');
        $this->assertSame(null, $subject->transform(null));
    }

    /**
     * @test
     */
    public function passingAnEmptyStringReturnsNull()
    {
        $subject = new IntegerTransformer('.', '');
        $this->assertSame(null, $subject->transform(''));
    }

    /**
     * @test
     */
    public function aFloatIsRegardedAsValidIfItDoesNotHaveFractionDigits()
    {
        $subject = new IntegerTransformer('.', ',');
        $this->assertSame(-12345, $subject->transform('-12345.0'));
        $this->assertSame(-12345, $subject->transform('-12,345.0000'));
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 16
     */
    public function aFloatIsRegardedAsInvValid()
    {
        $subject = new IntegerTransformer('.', ',');
        $this->assertSame(-12345, $subject->transform('-1.23'));
    }
}

<?php

namespace BlueM\Validation\Transformer;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Transformer\HourMinuteTransformer
 *
 * @covers BlueM\Validation\Transformer\HourMinuteTransformer
 */
class HourMinuteTransformerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function passingNullReturnsNull()
    {
        $subject = new HourMinuteTransformer('');
        $this->assertSame(null, $subject->transform(null));
    }

    /**
     * @test
     */
    public function passingAnEmptyStringReturnsNull()
    {
        $subject = new HourMinuteTransformer('');
        $this->assertSame(null, $subject->transform(''));
    }

    /**
	 * @test
	 */
	public function anHourWithoutMinutesWithoutPeriodIsParsedCorrectly()
	{
        $subject = new HourMinuteTransformer(true);
        $this->assertSame('16:00', $subject->transform('16'));
	}

    /**
	 * @test
	 */
	public function anHourWithoutMinutesButWithPeriodIsParsedCorrectly()
	{
        $subject = new HourMinuteTransformer(true);
        $this->assertSame('14:00', $subject->transform('2 pm'));
	}

    /**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 22
	 */
	public function a24HourTimeWithPeriodIsNotAccepted()
	{
        $subject = new HourMinuteTransformer(true);
        $subject->transform('16:20 pm');
	}

    /**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 22
	 */
	public function aHourWithoutMinutesButWithPeriodIsNotAcceptedIfTheLanguageDoesNotAcceptAPeriod()
	{
        $subject = new HourMinuteTransformer(false);
        $subject->transform('2 pm');
	}

    /**
     * @test
     */
    public function anHourWithMinutesWithoutPeriodIsParsedCorrectlyWhenSeparatedByColon()
    {
        $subject = new HourMinuteTransformer(true);
        $this->assertSame('07:15', $subject->transform('7:15'));
    }

    /**
     * @test
     */
    public function anHourWithMinutesWithoutPeriodIsParsedCorrectlyWhenSeparatedByPeriod()
    {
        $subject = new HourMinuteTransformer(true);
        $this->assertSame('11:02', $subject->transform('11.02'));
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 22
     */
    public function anHourWithoutMinutesButWithSeparatedIsNotAccepted()
    {
        $subject = new HourMinuteTransformer(true);
        $subject->transform('15:');
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 22
     */
    public function anHourAbove59IsNotAccepted()
    {
        $subject = new HourMinuteTransformer(true);
        $subject->transform('60:15');
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 22
     */
    public function aMinuteAbove59IsNotAccepted()
    {
        $subject = new HourMinuteTransformer(true);
        $subject->transform('15:60');
    }
}

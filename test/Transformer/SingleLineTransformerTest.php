<?php

namespace BlueM\Validation\Transformer;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Transformer\SingleLineTransformer
 *
 * @covers BlueM\Validation\Transformer\SingleLineTransformer
 */
class SingleLineTransformerTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @test
	 */
	public function aMultilineStringIsTransformedToASingleLineTransformer()
	{
        $subject = new SingleLineTransformer();
        $actual = $subject->transform("Line1\rLine2\r\nLine3\n\nLine4 ");
        $this->assertSame("Line1 Line2 Line3 Line4 ", $actual);
	}

	/**
	 * @test
	 */
	public function aSinglelineStringIsNotModified()
	{
        $subject = new SingleLineTransformer();
        $actual = $subject->transform(' abc   def');
        $this->assertSame(' abc   def', $actual);
	}
}

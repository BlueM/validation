<?php

namespace BlueM\Validation\Transformer;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Transformer\NormalizeReturnTransformer
 *
 * @covers BlueM\Validation\Transformer\NormalizeReturnTransformer
 */
class NormalizeReturnTransformerTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @test
	 */
	public function testTheTransformation()
	{
        $subject = new NormalizeReturnTransformer();
        $actual = $subject->transform("Line1\rLine2\r\nLine3\n\r\nLine4\n");
        $this->assertSame("Line1\nLine2\nLine3\n\nLine4\n", $actual);
	}
}

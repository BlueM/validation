<?php

namespace BlueM\Validation\Transformer;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Transformer\NormalizeTransformer
 *
 * @covers BlueM\Validation\Transformer\NormalizeTransformer
 */
class NormalizeTransformerTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @test
	 */
	public function testTheTransformation()
	{
        $subject = new NormalizeTransformer();
        $actual = $subject->transform(json_decode('["\u0075\u0308"]')[0]); // Surrogate pair
        $this->assertSame('ü', $actual);
	}
}

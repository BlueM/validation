<?php

namespace BlueM\Validation\Transformer;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Transformer\DateTransformer
 *
 * @covers BlueM\Validation\Transformer\DateTransformer
 */
class DateTransformerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function passingNullReturnsNull()
    {
        $subject = new DateTransformer('');
        $this->assertSame(null, $subject->transform(null));
    }

    /**
     * @test
     */
    public function passingAnEmptyStringReturnsNull()
    {
        $subject = new DateTransformer('');
        $this->assertSame(null, $subject->transform(''));
    }

    /**
	 * @test
	 */
	public function aDateTransformerInYmdFormatIsParsedCorrectly()
	{
        $subject = new DateTransformer('m/d/Y');
        $this->assertSame('2013-05-09', $subject->transform('2013-05-09'));
	}

    /**
	 * @test
	 */
	public function aDateTransformerInEnglishFormatIsParsedCorrectly()
	{
        $subject = new DateTransformer('m/d/Y');
        $this->assertSame('2013-05-24', $subject->transform('05/24/2013'));
	}

    /**
	 * @test
	 */
	public function aDateTransformerInGermanFormatIsParsedCorrectly()
	{
        $subject = new DateTransformer('d.m.Y');
        $this->assertSame('2013-05-24', $subject->transform('24.05.2013'));
	}

    /**
	 * @test
	 */
	public function aDateTransformerWithTwoDigitsAsYearIsAcceptedAndParsedCorrectly()
	{
        $subject = new DateTransformer('d.m.Y');
        $this->assertSame('2013-05-24', $subject->transform('24.5.13'));
	}

    /**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 13
     */
	public function aFutureDateTransformerWithTwoDigitsAsYearIsRegardedAsInvalid()
	{
        $subject = new DateTransformer('m/d/Y');
        $subject->transform('10/31/25');
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 13
	 */
	public function aDateTransformerThatDoesNotMatchTheInputFormatIsRegardedAsInvalid()
	{
        $subject = new DateTransformer('d.m.Y');
        $subject->transform('24/04/2013');
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 14
	 */
	public function anInvalidDateTransformerIsRegardedAsInvalid()
	{
        $subject = new DateTransformer('d.m.Y');
        $subject->transform('30.02.2013');
	}
}

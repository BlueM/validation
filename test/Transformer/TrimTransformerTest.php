<?php

namespace BlueM\Validation\Transformer;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Transformer\TrimTransformer
 *
 * @covers BlueM\Validation\Transformer\TrimTransformer
 */
class TrimTransformerTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @test
	 */
	public function leadingAndTrailingWhitespaceIsTrimTransformermed()
	{
        $subject = new TrimTransformer();
        $actual = $subject->transform(" abc\n");
        $this->assertSame("abc", $actual);
	}
}

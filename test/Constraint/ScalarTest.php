<?php

namespace BlueM\Validation\Constraint;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Constraint\Scalar
 *
 * @covers BlueM\Validation\Constraint\Scalar
 */
class ScalarTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
	 * @test
	 */
	public function aScalarValueIsReturnedUnmodified()
	{
        $subject = new Scalar();
        $this->assertSame('abcd', $subject->check('abcd', false));
	}

	/**
	 * @test
	 */
	public function nullIsReturnedUnmodified()
	{
        $subject = new Scalar();
        $this->assertSame(null, $subject->check(null, false));
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 8
	 */
	public function aValueWhichIsNeitherAScalarNotNullThrowsAnException()
	{
        $subject = new Scalar();
        $subject->check(array(), false);
	}
}

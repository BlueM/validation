<?php

namespace BlueM\Validation\Constraint;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Constraint\Pattern
 *
 * @covers BlueM\Validation\Constraint\Pattern
 */
class PatternTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
	 * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Invalid pattern given
	 */
	public function theConstructorThrowsAnExceptionIfThePatternIsNotAString()
	{
        new Pattern(123);
	}

	/**
	 * @test
	 */
	public function aValueMatchingThePatternIsReturnedUnmodified()
	{
        $subject = new Pattern('/^[A-Z][a-z]*\d+$/');
        $this->assertSame('Foo12345', $subject->check('Foo12345', false));
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 17
	 */
	public function aValueWhichDoesNotMatchThePatternThrowsAnException()
	{
        $subject = new Pattern('/^[A-Z][a-z]*\d+$/');
        $subject->check('Foo', false);
    }

	/**
	 * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Invalid exception code
     */
	public function tryingToSetTheExceptionCodeToANonIntegerThrowsAnException()
	{
        new Pattern('/^[A-Z][a-z]*\d+$/', 'a');
    }

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 12345
     */
	public function aValueWhichDoesNotMatchThePatternThrowsAnExceptionWithACustomErrorMessage()
	{
        $subject = new Pattern('/^[A-Z][a-z]*\d+$/', 12345);
        $subject->check('Foo', false);
    }
}

<?php

namespace BlueM\Validation\Constraint;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Constraint\Minlength
 *
 * @covers BlueM\Validation\Constraint\Minlength
 */
class MinlengthTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
	 * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Invalid minimum length given
	 */
	public function theConstructorThrowsAnExceptionIfTheMinimumLengthIsSmallerThan1()
	{
        new Minlength(0);
	}

	/**
	 * @test
	 */
	public function aStringIsReturnedUnmodifiedIfNotShorterThanTheMinimumLength()
	{
        $subject = new Minlength(4);
        $this->assertSame('abcd', $subject->check('abcd', false));
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 21
	 */
	public function checkingAStringWhichIsShorterThanTheMinimumLengthThrowsAnException()
	{
        $subject = new Minlength(3);
        $subject->check('ab', false);
	}
}

<?php

namespace BlueM\Validation\Constraint;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Constraint\Valuelist
 *
 * @covers BlueM\Validation\Constraint\Valuelist
 */
class ValuelistTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
	 * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Invalid value list specification
	 */
	public function theConstructorThrowsAnExceptionIfTheValueListIsNotGivenAsAString()
	{
        new Valuelist(new \DateTimeZone('utc'));
	}

    /**
	 * @test
	 */
	public function theConstructorAcceptsTheValueListAsAnArray()
	{
        new Valuelist(array('a', 'b', 'c'));
	}

    /**
	 * @test
	 */
	public function theConstructorAcceptsTheValueListAsAWhitespaceSeparatedString()
	{
        new Valuelist('a b c');
	}

	/**
	 * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Valuelist definition contains less than 2 values
	 */
	public function anExceptionIsThrownIfTheValueListContainsLessThanTwoItems()
	{
        $subject = new Valuelist('A');
        $subject->check('abcd', false);
	}

	/**
	 * @test
	 */
	public function aValueWhichIsInTheValuelistIsReturnedUnmodified()
	{
        $subject = new Valuelist('a b c d');
        $this->assertSame('c', $subject->check('c', false));
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 3
	 */
	public function aValueWhichIsnotInTheValuelistThrowsAnException()
	{
        $subject = new Valuelist('a b c d');
        $subject->check('e', false);
	}
}

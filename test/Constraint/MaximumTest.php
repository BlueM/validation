<?php

namespace BlueM\Validation\Constraint;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Constraint\Maximum
 *
 * @covers BlueM\Validation\Constraint\Maximum
 */
class MaximumTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
	 * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Invalid maximum value given
	 */
	public function theConstructorThrowsAnExceptionIfTheArgumentIsNotAScalar()
	{
        new Maximum(null);
	}

	/**
	 * @test
	 */
	public function aStringIsReturnedUnmodifiedIfItEqualsTheMaximum()
	{
        $subject = new Maximum('a');
        $this->assertSame('a', $subject->check('a', false));
	}

	/**
	 * @test
	 */
	public function aStringIsReturnedUnmodifiedIfItIsSmallerThanTheMaximum()
	{
        $subject = new Maximum('b');
        $this->assertSame('a', $subject->check('a', false));
	}

	/**
	 * @test
	 */
	public function anIntegerIsReturnedUnmodifiedIfItEqualsTheMaximum()
	{
        $subject = new Maximum(123);
        $this->assertSame(123, $subject->check(123, false));
	}

	/**
	 * @test
	 */
	public function anIntegerIsReturnedUnmodifiedIfItIsSmallerThanTheMaximum()
	{
        $subject = new Maximum(124);
        $this->assertSame(123, $subject->check(123, false));
	}

	/**
	 * @test
	 */
	public function aFloatIsReturnedUnmodifiedIfItEqualsTheMaximum()
	{
        $subject = new Maximum(1.23);
        $this->assertSame(1.23, $subject->check(1.23, false));
	}

	/**
	 * @test
	 */
	public function aFloatIsReturnedUnmodifiedIfItIsSmallerThanTheMaximum()
	{
        $subject = new Maximum(1.23);
        $this->assertSame(1.22, $subject->check(1.22, false));
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 2
	 */
	public function aStringWhichIsLargerThanTheMaximumThrowsAnException()
	{
        $subject = new Maximum('b');
        $subject->check('c', false);
	}
	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 2
	 */
	public function anIntegerWhichIsLargerThanTheMaximumThrowsAnException()
	{
        $subject = new Maximum(5);
        $subject->check(9, false);
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 2
	 */
	public function aFloatWhichIsLargerThanTheMaximumThrowsAnException()
	{
        $subject = new Maximum(1);
        $subject->check(1.001, false);
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 2
     */
	public function anotherPropertysValueCanBeUsedAsMaximum()
	{
        $dummy = new \StdClass;
        $dummy->a = '123';
        $subject = new Maximum('$a');
        $subject->check(456, $dummy);
	}
}

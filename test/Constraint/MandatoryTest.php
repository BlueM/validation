<?php

namespace BlueM\Validation\Constraint;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Constraint\Mandatory
 *
 * @covers BlueM\Validation\Constraint\Mandatory
 */
class MandatoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
     * @test
     */
    public function theClassDoesNotSkipEmptyValues()
    {
        $subject = new Mandatory();
        $this->assertFalse($subject->skipsBlankValues());
    }

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 4
	 */
	public function checkingNullThrowsAConstraintException()
	{
        $subject = new Mandatory();
        $subject->check(null, false);
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 4
	 */
	public function checkingAnEmptyStringThrowsAConstraintException()
	{
        $subject = new Mandatory();
        $subject->check('', false);
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 4
	 */
	public function checkingAnEmptyArrayThrowsAConstraintException()
	{
        $subject = new Mandatory();
        $subject->check(array(), false);
	}

	/**
	 * @test
	 */
	public function aStringWhichIsNotEmptyIsReturnedUnmodified()
	{
        $subject = new Mandatory();
        $this->assertSame('a', $subject->check('a', false));
	}

	/**
	 * @test
	 */
	public function anArrayWhichIsNotEmptyIsReturnedUnmodified()
	{
        $subject = new Mandatory();
        $this->assertSame(array('a'), $subject->check(array('a'), false));
	}
}

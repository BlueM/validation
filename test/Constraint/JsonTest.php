<?php

namespace BlueM\Validation\Constraint;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Constraint\Json
 *
 * @covers BlueM\Validation\Constraint\Json
 */
class JsonTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
     * Tears down the fixture
     */
    public function tearDown()
    {
        unset($this->i18n);
    }

	/**
	 * @test
	 */
	public function validJsonIsReturnedUnmodified()
	{
        $json    = '[{"a": "A", "b": "B"}]';
        $subject = new Json($json);
        $this->assertSame($json, $subject->check($json, false));
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 26
	 */
	public function aStringWhichIsNotValidJSONThrowsAnException()
	{
        $json    = '[{"a": "A", "b": B"}]';
        $subject = new Json($json);
        $this->assertSame($json, $subject->check($json, false));
	}
}

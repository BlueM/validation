<?php

namespace BlueM\Validation\Constraint;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Constraint\Maxlength
 *
 * @covers BlueM\Validation\Constraint\Maxlength
 */
class MaxlengthTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
	 * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Invalid maximum length given
	 */
	public function theConstructorThrowsAnExceptionIfTheMaximumLengthIsSmallerThan1()
	{
        new Maxlength(0);
	}

	/**
	 * @test
	 */
	public function aStringIsReturnedUnmodifiedIfNotLongerThanTheMaximumLength()
	{
        $subject = new Maxlength(4);
        $this->assertSame('abcd', $subject->check('abcd', false));
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 20
	 */
	public function checkingAStringWhichIsLongerThanTheMaximumLengthThrowsAnException()
	{
        $subject = new Maxlength(3);
        $subject->check('abcd', false);
	}
}

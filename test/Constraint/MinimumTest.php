<?php

namespace BlueM\Validation\Constraint;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Constraint\Minimum
 *
 * @covers BlueM\Validation\Constraint\Minimum
 */
class MinimumTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
	 * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Invalid minimum value given
	 */
	public function theConstructorThrowsAnExceptionIfTheArgumentIsNotAScalar()
	{
        new Minimum(null);
	}

	/**
	 * @test
	 */
	public function aStringIsReturnedUnmodifiedIfItEqualsTheMinimum()
	{
        $subject = new Minimum('a');
        $this->assertSame('a', $subject->check('a'));
	}

	/**
	 * @test
	 */
	public function aStringIsReturnedUnmodifiedIfItIsLargerThanTheMinimum()
	{
        $subject = new Minimum('a');
        $this->assertSame('b', $subject->check('b'));
	}

	/**
	 * @test
	 */
	public function anIntegerIsReturnedUnmodifiedIfItEqualsTheMinimum()
	{
        $subject = new Minimum(123);
        $this->assertSame(123, $subject->check(123));
	}

	/**
	 * @test
	 */
	public function anIntegerIsReturnedUnmodifiedIfItIsLargerThanTheMinimum()
	{
        $subject = new Minimum(123);
        $this->assertSame(124, $subject->check(124));
	}

	/**
	 * @test
	 */
	public function aFloatIsReturnedUnmodifiedIfItEqualsTheMinimum()
	{
        $subject = new Minimum(1.23);
        $this->assertSame(1.23, $subject->check(1.23));
	}

	/**
	 * @test
	 */
	public function aFloatIsReturnedUnmodifiedIfItIsLargerThanTheMinimum()
	{
        $subject = new Minimum(1.23);
        $this->assertSame(1.24, $subject->check(1.24));
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 1
	 */
	public function aStringWhichIsSmallerThanTheMinimumThrowsAnException()
	{
        $subject = new Minimum('b');
        $subject->check('a');
	}
	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 1
	 */
	public function anIntegerWhichIsSmallerThanTheMinimumThrowsAnException()
	{
        $subject = new Minimum(5);
        $subject->check(3);
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 1
	 */
	public function aFloatWhichIsSmallerThanTheMinimumThrowsAnException()
	{
        $subject = new Minimum(1.23);
        $subject->check(1);
	}

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 1
     */
    public function anotherPropertysValueCanBeUsedAsMinimum()
    {
        $dummy    = new \StdClass;
        $dummy->a = '456';
        $subject  = new Minimum('$a');
        $subject->check(123, $dummy, $subject);
    }
}

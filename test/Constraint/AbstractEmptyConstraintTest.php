<?php

namespace BlueM\Validation\Constraint;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Constraint\AbstractEmptyConstraint
 *
 * @covers BlueM\Validation\Constraint\AbstractEmptyConstraint
 */
class AbstractEmptyConstraintTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var AbstractEmptyConstraint
     */
    protected $subject;

    /**
     * @var \ReflectionMethod
     */
    protected $method;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->subject = $this->getMockBuilder(__NAMESPACE__ . '\AbstractEmptyConstraint')
            ->disableOriginalConstructor()
            ->setMethods(['__construct'])
            ->getMockForAbstractClass();

        $this->method = new \ReflectionMethod($this->subject, 'getReferenceValue');
        $this->method->setAccessible(true);
    }

    /**
     * Tears down the fixture
     */
    public function tearDown()
    {
        unset($this->subject);
    }


    /**
     * @test
     */
    public function nullIsRegardedAsBlank()
    {
        $reflm   = new \ReflectionMethod($this->subject, 'isBlank');
        $reflm->setAccessible(true);
        $this->assertTrue($reflm->invoke($this->subject, null));
    }

    /**
     * @test
     */
    public function aStringContainingOnlyWhitespaceIsNotRegardedAsBlank()
    {
        $reflm   = new \ReflectionMethod($this->subject, 'isBlank');
        $reflm->setAccessible(true);
        $this->assertFalse($reflm->invoke($this->subject, ' '));
    }

    /**
     * @test
     */
    public function zeroIsNotRegardedAsBlank()
    {
        $reflm   = new \ReflectionMethod($this->subject, 'isBlank');
        $reflm->setAccessible(true);
        $this->assertFalse($reflm->invoke($this->subject, 0));
    }

    /**
     * @test
     */
    public function anEmptyStringIsRegardedAsBlank()
    {
        $reflm   = new \ReflectionMethod($this->subject, 'isBlank');
        $reflm->setAccessible(true);
        $this->assertTrue($reflm->invoke($this->subject, ''));
    }

    /**
     * @test
     */
    public function anEmptyArrayIsRegardedAsBlank()
    {
        $reflm   = new \ReflectionMethod($this->subject, 'isBlank');
        $reflm->setAccessible(true);
        $this->assertTrue($reflm->invoke($this->subject, array()));
    }

    /**
	 * @test
     * @expectedException RuntimeException
     * @expectedExceptionMessage references a non-existing property
	 */
	public function aLiteralValueWithoutDollarPrefixThrowsAnException()
	{
        $actual = $this->method->invoke($this->subject, '123', null, null);
        $this->assertSame('123', $actual);
    }

	/**
	 * @test
     * @expectedException RuntimeException
     * @expectedExceptionMessage references a non-existing property abc
	 */
	public function aReferenceToAPropertyThrowsAnExceptionIfThereIsNoSuchProperty()
	{
        $object = new \StdClass();
        $actual = $this->method->invoke($this->subject, 'abc', $object, null);
        $this->assertSame(null, $actual);
    }

	/**
	 * @test
	 */
	public function aReferenceToAnExistingPropertyReturnsThePropertysValue()
	{
        eval('namespace ' . __NAMESPACE__ . '; class Dummy { protected $foo = "Bar"; }');
        $object = new Dummy();
        $actual = $this->method->invoke($this->subject, 'foo', $object, null);
        $this->assertSame(array('Bar', '$foo'), $actual);
    }

	/**
	 * @test
     * @expectedException RuntimeException
     * @expectedExceptionMessage references a key 'abc' in a non-collection
	 */
	public function aReferenceToACollectionItemThrowsAnExceptionIfTheGivenCollectionIsNotAnArray()
	{
        $object = new \StdClass();
        $actual = $this->method->invoke($this->subject, '$abc', $object, '');
        $this->assertSame(null, $actual);
    }

	/**
	 * @test
     * @expectedException RuntimeException
     * @expectedExceptionMessage references a non-existing key 'abc' in collection
	 */
	public function aReferenceToACollectionItemThrowsAnExceptionIfThereIsNoCollectionItemWithThatName()
	{
        $object = new \StdClass();
        $actual = $this->method->invoke($this->subject, '$abc', $object, ['def' => 'ghi']);
        $this->assertSame(null, $actual);
    }

	/**
	 * @test
	 */
	public function aReferenceToACollectionItemReturnsTheCollectionItemsValue()
	{
        $object = new \StdClass();
        $actual = $this->method->invoke($this->subject, '$abc', $object, ['abc' => '12345']);
        $this->assertSame(array('12345', '$$abc'), $actual);
    }
}

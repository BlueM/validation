<?php

namespace BlueM\Validation\Constraint;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Constraint\Blank
 *
 * @covers BlueM\Validation\Constraint\Blank
 */
class BlankTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
     * Tears down the fixture
     */
    public function tearDown()
    {
        unset($this->i18n);
    }

    /**
     * @test
     */
    public function theClassDoesNotSkipEmptyValues()
    {
        $subject = new Blank('IF abc');
        $this->assertFalse($subject->skipsBlankValues());
    }

    /**
     * @test
     */
    public function aValueWhichPassesValidationIsReturnedUnmodified()
    {
        $subject = new Blank();
        $this->assertSame(array(), $subject->check(array()));
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     */
    public function ifTrueIsPassedAsTheConstraintConditionItIsIgnored()
    {
        $subject = $this->getMockBuilder(__NAMESPACE__ . '\Blank')
            ->setConstructorArgs(array(true))
            ->setMethods(array('checkCondition'))
            ->getMock();
        $subject->expects($this->never())
            ->method('checkCondition');

        $this->assertSame('', $subject->check('Foobar'));
    }

    /**
     * @test
     */
    public function ifTheValueIsBlankTheValidationPassesRegardlessOfTheCondition()
    {
        $subject = new Blank('asdasdsa');
        $subject->check('');
    }

    /**
     * @test
     * @expectedException RuntimeException
     * @expectedExceptionMessage Invalid condition
     */
    public function ifTheConditionIsInvalidAnExceptionIsThrown()
    {
        $subject = new Blank('asdasdsa');
        $subject->check('aaaaa');
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 6
     */
    public function aValueMustBeBlankIfTheReferenceValueOfAPositiveConditionIsNotBlank()
    {
        $dummyObject = new \StdClass;

        $subject = $this->getMockBuilder(__NAMESPACE__ . '\Blank')
            ->setConstructorArgs(array('IF $property'))
            ->setMethods(array('getReferenceValue'))
            ->getMock();
        $subject->expects($this->once())
            ->method('getReferenceValue')
            ->with('property', $dummyObject)
            ->will($this->returnValue(array('Not blank', '$property')));

        $subject->check('Foobar', $dummyObject);
    }

    /**
     * @test
     */
    public function aValuePassesValidationIfTheReferenceValueOfAPositiveConditionIsBlank()
    {
        $dummyObject = new \StdClass;

        $subject = $this->getMockBuilder(__NAMESPACE__ . '\Blank')
            ->setConstructorArgs(array('IF $property'))
            ->setMethods(array('getReferenceValue'))
            ->getMock();
        $subject->expects($this->once())
            ->method('getReferenceValue')
            ->with('property', $dummyObject)
            ->will($this->returnValue(array('', '$property')));

        $this->assertSame('Foobar', $subject->check('Foobar', $dummyObject));
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 12
     */
    public function aValueMustBeBlankIfTheReferenceValueOfANegativeConditionIsBlank()
    {
        $dummyObject = new \StdClass;

        $subject = $this->getMockBuilder(__NAMESPACE__ . '\Blank')
            ->setConstructorArgs(array('IF NOT $property'))
            ->setMethods(array('getReferenceValue'))
            ->getMock();
        $subject->expects($this->once())
            ->method('getReferenceValue')
            ->with('property', $dummyObject)
            ->will($this->returnValue(array('', '$property')));

        $this->assertSame('', $subject->check('Foobar', $dummyObject));
    }

    /**
     * @test
     */
    public function aValuePassesValidationIfTheReferenceValueOfANegativeConditionIsNotBlank()
    {
        $dummyObject = new \StdClass;

        $subject = $this->getMockBuilder(__NAMESPACE__ . '\Blank')
            ->setConstructorArgs(array('IF NOT $property'))
            ->setMethods(array('getReferenceValue'))
            ->getMock();
        $subject->expects($this->once())
            ->method('getReferenceValue')
            ->with('property', $dummyObject)
            ->will($this->returnValue(array('Not blank', '$property')));

        $this->assertSame('Foobar', $subject->check('Foobar', $dummyObject));
    }
}

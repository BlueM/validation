<?php

namespace BlueM\Validation\Constraint;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Constraint\Wellformed
 *
 * @covers BlueM\Validation\Constraint\Wellformed
 */
class WellformedTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

	/**
	 * @test
	 */
	public function aWellformedStringIsReturnedUnmodified()
	{
        $xml     = '<a><b>B</b><c /></a>';
        $subject = new Wellformed($xml);
        $this->assertSame($xml, $subject->check($xml, false));
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 25
	 */
	public function aNotWellformedStringThrowsAnException()
	{
        $xml     = '<a><b>B</b><c></a>';
        $subject = new Wellformed($xml);
        $this->assertSame($xml, $subject->check($xml, false));
	}
}

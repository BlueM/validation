<?php

namespace BlueM\Validation\Constraint;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Constraint\AbstractLimitConstraint
 *
 * @covers BlueM\Validation\Constraint\AbstractLimitConstraint
 */
class AbstractLimitConstraintTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var AbstractLimitConstraint
     */
    protected $subject;

    /**
     * @var \ReflectionMethod
     */
    protected $method;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->subject = $this->getMockBuilder(__NAMESPACE__ . '\AbstractLimitConstraint')
            ->disableOriginalConstructor()
            ->setMethods(['__construct'])
            ->getMockForAbstractClass();

        $this->method = new \ReflectionMethod($this->subject, 'getLimit');
        $this->method->setAccessible(true);
    }

    /**
     * Tears down the fixture
     */
    public function tearDown()
    {
        unset($this->subject);
    }

	/**
	 * @test
	 */
	public function aLiteralValueIsReturnedUnmodified()
	{
        $actual = $this->method->invoke($this->subject, '123', null, null);
        $this->assertSame('123', $actual);
    }

	/**
	 * @test
     * @expectedException RuntimeException
     * @expectedExceptionMessage references a non-existing property abc
	 */
	public function aReferenceToAPropertyThrowsAnExceptionIfThereIsNoSuchProperty()
	{
        $object = new \StdClass();
        $actual = $this->method->invoke($this->subject, '$abc', $object, null);
        $this->assertSame(null, $actual);
    }

	/**
	 * @test
	 */
	public function aReferenceToAnExistingPropertyReturnsThePropertysValue()
	{
        $object = new \StdClass();
        $object->abc = 4711;
        $actual = $this->method->invoke($this->subject, '$abc', $object, null);
        $this->assertSame(4711, $actual);
    }

	/**
	 * @test
     * @expectedException RuntimeException
     * @expectedExceptionMessage references a key 'abc' in a non-collection
	 */
	public function aReferenceToACollectionItemThrowsAnExceptionIfTheGivenCollectionIsNotAnArray()
	{
        $object = new \StdClass();
        $actual = $this->method->invoke($this->subject, '$$abc', $object, '');
        $this->assertSame(null, $actual);
    }

	/**
	 * @test
     * @expectedException RuntimeException
     * @expectedExceptionMessage references a non-existing key 'abc' in collection
	 */
	public function aReferenceToACollectionItemThrowsAnExceptionIfThereIsNoCollectionItemWithThatName()
	{
        $object = new \StdClass();
        $actual = $this->method->invoke($this->subject, '$$abc', $object, ['def' => 'ghi']);
        $this->assertSame(null, $actual);
    }

	/**
	 * @test
	 */
	public function aReferenceToACollectionItemReturnsTheItemsValue()
	{
        $object = new \StdClass();
        $actual = $this->method->invoke($this->subject, '$$abc', $object, ['abc' => '12345']);
        $this->assertSame('12345', $actual);
    }
}

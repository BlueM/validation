<?php

namespace BlueM\Validation\Constraint;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Constraint\Decimalplaces
 *
 * @covers BlueM\Validation\Constraint\Decimalplaces
 */
class DecimalplacesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
	 * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Invalid number of decimal places
	 */
	public function theConstructorThrowsAnExceptionIfAnInvaludNumberOfDecimalPlacesIsGiven()
	{
        new Decimalplaces(0);
	}

	/**
	 * @test
	 */
	public function aFloatIsReturnedUnmodifiedIfItHasNoMoreThanTheMaximumNumberOfDecimalPlaces()
	{
        $subject = new Decimalplaces(4);
        $this->assertSame(1.1234, $subject->check(1.1234, false));
	}

	/**
	 * @test
	 */
	public function trailingZerosAreIgnoredWhenCheckingTheMaximumNumberOfDecimalPlaces()
	{
        $subject = new Decimalplaces(4);
        $this->assertSame(1.1234, $subject->check(1.12340000, false));
	}

	/**
	 * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Expected value to be a float
	 */
	public function aValueWhichIsNotAFloatThrowsAnException()
	{
        $subject = new Decimalplaces(3);
        $subject->check("1.234", false);
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 15
	 */
	public function checkingAFloatWhichMoreThanTheMaximumNumberOfDecimalPlacesThrowsAnException()
	{
        $subject = new Decimalplaces(3);
        $subject->check(1.1234, false);
	}
}

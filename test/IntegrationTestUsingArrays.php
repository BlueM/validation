<?php

use BlueM\Validation\I18n\En;
use BlueM\Validation\Validator;

require_once __DIR__ . '/bootstrap.php';

/**
 * Integration tests for BlueM\Validation
 * @codeCoverageIgnore
 */
class IntegrationTestUsingArrays extends \PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function validatingAnObjectSucceedsIfAllPropertiesAreValid()
    {
        $i18n = new En();

        $subject = new \StdClass;
        $subject->property1 = 'Abc';
        $subject->property2 = 123.45;
        $subject->property3 = 125;
        $subject->property4 = '2014-09-06';

        $validator = new Validator($i18n);

        $actual = $validator->validateObject(
            $subject,
            array(
                'property1' => array(
                    'type' => 'string',
                ),
                'property2' => array(
                    'type'          => 'float',
                    'decimalPlaces' => '2',
                ),
                'property3' => array(
                    'type'    => 'float',
                    'minimum' => '$property2',
                ),
                'property4' => array(
                    'type'    => 'date',
                    'maximum' => date('Y-m-d'),
                ),
            )
        );
        $this->assertInternalType('array', $actual);
        $this->assertEquals(array(), $actual);
    }

    /**
     * @test
     */
    public function validatingAnObjectReturnsAllValidationErrors()
    {
        $i18n = new En();

        $subject = new \StdClass;
        $subject->property1 = '4Abc';
        $subject->property2 = 123.45;
        $subject->property3 = 120;
        $subject->property4 = '2013-09-06';

        $validator = new Validator($i18n);

        $actual = $validator->validateObject(
            $subject,
            array(
                'property1' => array(
                    'type'    => 'string',
                    'pattern' => '/^[A-Z]/',
                ),
                'property2' => array(
                    'type'          => 'float',
                    'decimalPlaces' => '1',
                ),
                'property3' => array(
                    'type'    => 'float',
                    'minimum' => '$property2',
                ),
                'property4' => array(
                    'type'    => 'date',
                    'minimum' => '2014-09-06',
                ),
            )
        );

        $this->assertInternalType('array', $actual);

        $this->assertEquals(
            array(
                'property1' => 'The value is formally invalid.',
                'property2' => 'The number may not have more than 1 decimal places.',
                'property3' => 'The value must not be smaller than 123.45.',
                'property4' => 'The value must not be smaller than 09/06/2014.',
            ),
            $actual
        );
    }

    /**
     * @test
     */
    public function validatingAValueAlsoValidatesDependencyConstraints()
    {
        $i18n = new En();

        $validator = new Validator($i18n);

        try {
            $validator->validate(
                array(
                    'type' => 'array[]',
                    'type:field1' => 'string',
                    'type:field2' => 'int',
                    'type:field3' => 'int',
                    'type:field4' => 'int',
                    'minimum:field3' => '$$field2',
                    'mandatory:field4' => 'IF $$field2',
                ),
                array(
                    array(
                        'field1' => 'FOo',
                        'field2' => 123,
                        'field3' => 37,
                    )
                ),
                array(
                    '$$field2' => 'Field_2',
                )
            );
        } catch (\BlueM\Validation\InvalidCollectionArgumentException $e) {
            $this->assertSame(
                array(
                    0 => array(
                        'field3' => 'The value must not be smaller than 123.',
                        'field4' => 'This value is mandatory, if field Field_2 is not empty.',
                    )
                ),
                $e->getErrors()
            );
        }
    }

    /**
     * @test
     */
    public function settingMandatoryConstraintToFalseIsInterpretedCorrectly()
    {
        $i18n = new En();

        $validator = new Validator($i18n);

        $actual = $validator->validate(
            array(
                'type'      => 'string',
                'mandatory' => false,
            ),
            ''
        );

        $this->assertSame('', $actual);
    }
}

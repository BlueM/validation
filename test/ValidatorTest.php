<?php

namespace BlueM\Validation;

use ReflectionProperty;
use ReflectionClass;

require_once __DIR__ . '/bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Validator
 *
 * @covers BlueM\Validation\Validator
 */
class ValidatorTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Argument 1 must be an object
     */
    public function tryingToValidateAnObjectThrowsAnExceptionIfANonObjectIsGiven()
    {
        $i18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->getMockForAbstractClass();

        $subject = new Validator($i18nMock);
        $subject->validateObject('scalar');
    }

    /**
     * @test
     */
    public function validateAValidObject()
    {
        $i18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->getMockForAbstractClass();

        $validatorMock = $this->getMockBuilder(__NAMESPACE__ . '\Validator')
            ->setConstructorArgs(array($i18nMock))
            ->setMethods(array('validate'))
            ->getMock();
        $validatorMock->expects($this->exactly(3))
            ->method('validate')
            ->will($this->returnArgument(0));

        $dummyObj = new DummyDomainObject();
        $dummyObj->foo   = 'Foobar';
        $dummyObj->email = 'somebody@example.com';

        $actual = $validatorMock->validateObject($dummyObj);
        $this->assertSame(array(), $actual);
    }

    /**
     * @test
     */
    public function validateAnInvalidObject()
    {
        $i18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->getMockForAbstractClass();
        $i18nMock->expects($this->once())
            ->method('getStrings')
            ->will($this->returnValue(array('x' => 'y')));

        $dummyObj = new DummyDomainObject();
        $dummyObj->foo   = array();
        $dummyObj->email = 'Not an e-mail address';

        $subject = new Validator($i18nMock);
        $errors = $subject->validateObject($dummyObj);
        $this->assertEquals(
            array(
                'foo'   => ValidationFailure::WRONG_TYPE,
                'email' => ValidationFailure::NOT_EMAIL,
            ),
            $errors
        );
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Argument 1 must be an object
     */
    public function tryingToValidateAPropertyValueThrowsAnExceptionIfANonObjectIsGiven()
    {
        $i18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->getMockForAbstractClass();

        $subject = new Validator($i18nMock);
        $subject->validatePropertyValue('', '', '');
    }

    /**
     * @test
     */
    public function validateAPropertyValue()
    {
        $dummyObj = new DummyDomainObject();

        $i18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->getMockForAbstractClass();

        $validatorMock = $this->getMockBuilder(__NAMESPACE__ . '\Validator')
            ->setConstructorArgs(array($i18nMock))
            ->setMethods(array('validate'))
            ->getMock();

        $constraints = array(
            'type'      => 'string',
            'maxlength' => 40,
            'mandatory' => true,
            'pattern'   => '/^Foo/',
        );

        $validatorMock->expects($this->once())
            ->method('validate')
            ->with($constraints, 'Value')
            ->will($this->returnValue('Return value'));

        $actual = $validatorMock->validatePropertyValue($dummyObj, 'foo', 'Value');

        $this->assertSame('Return value', $actual);
    }

    /**
     * @test
     */
    public function validateAPropertyUsingAnnotations()
    {
        $dummyObj = new DummyDomainObject();
        $dummyObj->foo = 'Bar';

        $i18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->getMockForAbstractClass();

        $validatorMock = $this->getMockBuilder(__NAMESPACE__ . '\Validator')
            ->setConstructorArgs(array($i18nMock))
            ->setMethods(array('validate'))
            ->getMock();

        $constraints = array(
            'type'      => 'string',
            'maxlength' => 40,
            'mandatory' => true,
            'pattern'   => '/^Foo/',
        );

        $validatorMock->expects($this->once())
            ->method('validate')
            ->with($constraints, 'Bar')
            ->will($this->returnValue('Return value'));

        $actual = $validatorMock->validateProperty($dummyObj, 'foo', null);

        $this->assertSame('Return value', $actual);
    }

    /**
     * @test
     */
    public function validateAPropertyUsingAnnotationsGivenAsArgument()
    {
        $dummyObj = new DummyDomainObject();
        $dummyObj->foo = 'Bar';

        $i18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->getMockForAbstractClass();

        $validatorMock = $this->getMockBuilder(__NAMESPACE__ . '\Validator')
            ->setConstructorArgs(array($i18nMock))
            ->setMethods(array('validate'))
            ->getMock();

        $constraints = array(
            'type'      => 'string',
            'maxlength' => 20,  // <-- differs from annotation
            'mandatory' => true,
            'pattern'   => '/^Foo/',
        );

        $validatorMock->expects($this->once())
            ->method('validate')
            ->with($constraints, 'Bar')
            ->will($this->returnValue('Return value'));

        $actual = $validatorMock->validateProperty($dummyObj, 'foo', $constraints);

        $this->assertSame('Return value', $actual);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Argument 1 must be an object
     */
    public function tryingToValidateAPropertyThrowsAnExceptionIfArgument1IsNotAnObject()
    {
        $i18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->getMockForAbstractClass();

        $validatorMock = $this->getMockBuilder(__NAMESPACE__ . '\Validator')
            ->setConstructorArgs(array($i18nMock))
            ->setMethods(array('validate'))
            ->getMock();

        $validatorMock->validateProperty('Not an object', 'foo');
    }

    /**
     * @test
     * @expectedException RuntimeException
     * @expectedExceptionMessage The type must be defined
     */
    public function tryingToValidateAValueThrowsAnExceptionIfNoTypeIsGiven()
    {
        $i18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->getMockForAbstractClass();
        $subject = new Validator($i18nMock);
        $subject->validate(array(), 'value');
    }

    /**
     * @test
     * @expectedException RuntimeException
     * @expectedExceptionMessage Unknown type
     */
    public function tryingToValidateAValueThrowsAnExceptionIfAnInvalidTypeIsGiven()
    {
        $i18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->getMockForAbstractClass();
        $subject = new Validator($i18nMock);
        $subject->validate(array('type' => 'nonexistent'), 'value');
    }

    /**
     * @test
     * @expectedException RuntimeException
     * @expectedExceptionMessage Unsupported constraint
     */
    public function tryingToValidateAnObjectThrowsAnExceptionIfAnInvalidConstraintIsGiven()
    {
        $i18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->getMockForAbstractClass();

        $subject = new Validator($i18nMock);
        $subject->validate(
            array(
                'type'               => 'string',
                'no-such-constraint' => 'foo',
            ),
            'The value'
        );
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Argument 1 must be a string
     */
    public function tryingToAddANamespaceThrowsAnExceptionIfTheNamespaceIsNotAString()
    {
        $i18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->getMockForAbstractClass();
        $subject  = new Validator($i18nMock);
        $subject->addNamespace(array());
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Namespace already registered
     */
    public function tryingToAddANamespaceThrowsAnExceptionIfTheNamespaceHasAlreadyBeenRegistered()
    {
        $i18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->getMockForAbstractClass();
        $subject  = new Validator($i18nMock);
        $subject->addNamespace('Foo\Bar');
        $subject->addNamespace('Foo\Bar');
    }

    /**
     * @test
     */
    public function addANamespaceWithoutAdditionalArguments()
    {
        $i18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->getMockForAbstractClass();
        $subject  = new Validator($i18nMock);
        $subject->addNamespace('Foo\Bar');

        $namespacesProperty = new \ReflectionProperty($subject, 'namespaces');
        $namespacesProperty->setAccessible(true);
        $actual = $namespacesProperty->getValue($subject);
        $this->assertArrayHasKey('Foo\Bar', $actual);
        $this->assertSame(array(), $actual['Foo\Bar']);
    }

    /**
     * @test
     */
    public function setTheI18nInstance()
    {
        $i18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->getMockForAbstractClass();
        $subject  = new Validator($i18nMock);

        $otherI18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->getMockForAbstractClass();
        $subject->setI18n($otherI18nMock);

        $i18nProperty = new \ReflectionProperty($subject, 'i18n');
        $i18nProperty->setAccessible(true);
        $actual = $i18nProperty->getValue($subject);
        $this->assertSame($otherI18nMock, $actual);
    }

    /**
     * @test
     */
    public function addANamespaceWithAdditionalArguments()
    {
        $i18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->getMockForAbstractClass();
        $subject  = new Validator($i18nMock);
        $subject->addNamespace('Foo\Bar', array('Hello', 'world'));

        $namespacesProperty = new \ReflectionProperty($subject, 'namespaces');
        $namespacesProperty->setAccessible(true);
        $actual = $namespacesProperty->getValue($subject);
        $this->assertArrayHasKey('Foo\Bar', $actual);
        $this->assertSame(array('Hello', 'world'), $actual['Foo\Bar']);
    }

    /**
     * @test
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Could not find a class named
     */
    public function tryingToGetASingleConstraintFromAnAnnotationThrowsAnExceptionIfTheClassIsInvalid()
    {
        Validator::getConstraint('NoSuchClass', 'email', 'label');
    }

    /**
     * @test
     */
    public function getASingleConstraintFromAnAnnotation()
    {
        $label = Validator::getConstraint(
            __NAMESPACE__ . '\DummyDomainObject',
            'email',
            'label'
        );
        $this->assertSame('E-mail address', $label);
    }

    /**
     * @test
     */
    public function getCheckableProperties()
    {
        $rc = new ReflectionClass(__NAMESPACE__ . '\DummyDomainObject');

        $i18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->getMockForAbstractClass();

        $subject = new Validator($i18nMock);
        $reflm = new \ReflectionMethod($subject, 'getCheckableProperties');
        $reflm->setAccessible(true);
        $actual = $reflm->invoke($subject, $rc);

        $this->assertInternalType('array', $actual);
        $this->assertSame(3, count($actual));

        $this->assertInternalType('array', $actual['foo']);
        $this->assertInternalType('array', $actual['email']);

        $this->assertInstanceOf('ReflectionProperty', $actual['foo']['property']);
        $this->assertInstanceOf('ReflectionProperty', $actual['email']['property']);

        $this->assertInternalType('array', $actual['foo']['constraints']);
        $this->assertInternalType('array', $actual['email']['constraints']);

        $this->assertSame(
            array(
                'type' => 'string',
                'maxlength' => '40',
                'mandatory' => true,
                'pattern' => '/^Foo/',
            ),
            $actual['foo']['constraints']
        );

        $this->assertSame(
            array(
                'type' => 'email',
                'label' => 'E-mail address',
            ),
            $actual['email']['constraints']
        );

        $this->assertSame(
            array(
                'type' => 'float',
                'minimum' => '0',
                'maximum' => '100',
            ),
            $actual['number']['constraints']
        );
    }
}

/**
 * Class DummyDomainObject
 *
 * @package BlueM\Validation
 */
class DummyDomainObject
{
    /**
     * @validation-type      string
     * @validation-maxlength 40
     * @validation-mandatory
     * @validation-pattern   /^Foo/
     */
    public $foo;

    /**
     * @validation-type  email
     * @validation-label E-mail address
     */
    public $email;

    /**
     * @var float
     * @validation-type float
     * @validation-minimum 0
     * @validation-maximum 100
     */
    public $number;

    /**
     * This is a property that should not be validated
     */
    public $somethingElse;
}

<?php

namespace BlueM\Validation\Type;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Tests BlueM\Validation\Type\HourMinuteType
 *
 * These are no unit tests (as the class rather contains configuration than code),
 * but tests the functionality of all the parts: type, transformers and constraints.
 *
 * @covers BlueM\Validation\Type\HourMinuteType
 */
class HourMinuteTypeTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    public function tearDown()
    {
        unset($this->i18n);
    }

    /**
	 * @test
	 */
	public function aValidNotLocalizedValueWithTrailingWhitespaceIsProcessedCorrectly()
	{
        $subject = new HourMinuteType($this->i18n, false);
        $this->assertSame('12:34', $subject->validate('12:34 '));
	}

    /**
	 * @test
	 */
	public function aValidLocalizedValueWithLeadingWhitespaceIsProcessedCorrectly()
	{
        $this->i18n->expects($this->once())
            ->method('acceptsPeriod')
            ->will($this->returnValue(true));

        $subject = new HourMinuteType($this->i18n, false);
        $this->assertSame('16:19', $subject->validate(' 4:19 pm'));
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 8
     */
	public function aNonScalarValueThrowsAnException()
	{
        $subject = new HourMinuteType($this->i18n, false);
        $subject->validate(array());
	}
}

<?php

namespace BlueM\Validation\Type;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Tests BlueM\Validation\Type\EmailType
 *
 * This is not a real unit test (as the class rather contains configuration than code),
 * but tests the functionality of all the parts: type, transformers and constraints.
 *
 * @covers BlueM\Validation\Type\EmailType
 */
class EmailTypeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
	 * @test
	 */
	public function aValidEmailTypeAddressIsProcessedCorrectly()
	{
        $subject = new EmailType();
        $this->assertSame(
            'foo@example.com',
            $subject->validate('foo@example.com'));
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 20
     */
	public function aValidEmailTypeAddressWithMoreThan254CharactersThrowsAnException()
	{
        $subject = new EmailType();
        $subject->validate(str_repeat('x', 243) . '@example.com');
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 18
	 */
	public function anInvalidEmailTypeAddressThrowsAnException()
	{
        $subject = new EmailType();
        $subject->validate('foo@example');
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 18
	 */
	public function anInvalidEmailTypeAddressThrowsAnException2()
	{
        $subject = new EmailType();
        $subject->validate('foo@example@example.loc');
	}
}

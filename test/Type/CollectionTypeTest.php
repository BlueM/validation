<?php

namespace BlueM\Validation\Type;

use BlueM\Validation\InvalidCollectionArgumentException;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Tests BlueM\Validation\Type\CollectionType. This is not a real unit test (as the class rather
 * contains configuration than code), but tests the functionality of all the parts:
 * type, transformers and constraints.
 *
 * @covers BlueM\Validation\Type\CollectionType
 */
class CollectionTypeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function createAnInstance()
    {
        $validatorMock = $this->getMockBuilder('BlueM\Validation\Validator')
            ->disableOriginalConstructor()
            ->getMock();

        $dummyConstraints = ['whatever'];
        $subject = new CollectionType($validatorMock, $dummyConstraints);

        $validatorProperty = new \ReflectionProperty($subject, 'validator');
        $validatorProperty->setAccessible(true);
        $this->assertSame($validatorMock, $validatorProperty->getValue($subject));

        $constraintsProperty = new \ReflectionProperty($subject, 'constraints');
        $constraintsProperty->setAccessible(true);
        $this->assertSame($dummyConstraints, $constraintsProperty->getValue($subject));
    }

    /**
     * @test
     */
    public function performSuccessfulValidation()
    {
        $validatorMock = $this->getMockBuilder('BlueM\Validation\Validator')
            ->disableOriginalConstructor()
            ->getMock();
        $validatorMock->expects($this->exactly(3))
            ->method('validate')
            ->will($this->onConsecutiveCalls('a', 'b-transformed', 'c'));

        $subject = $this->getMockBuilder(__NAMESPACE__ . '\CollectionType')
            ->disableOriginalConstructor()
            ->setMethods(array('__construct'))
            ->getMock();
        $validatorProperty = new \ReflectionProperty($subject, 'validator');
        $validatorProperty->setAccessible(true);
        $validatorProperty->setValue($subject, $validatorMock);

        $constraintsProperty = new \ReflectionProperty($subject, 'constraints');
        $constraintsProperty->setAccessible(true);
        $constraintsProperty->setValue($subject, array());

        $actual = $subject->validate(array('a', 'b', 'c'));

        $this->assertSame(array('a', 'b-transformed', 'c'), $actual);
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\InvalidCollectionArgumentException
     * @expectedExceptionMessage Failed
     */
    public function performFailingValidation()
    {
        $validatorMock = $this->getMockBuilder('BlueM\Validation\Validator')
            ->disableOriginalConstructor()
            ->getMock();
        $validatorMock->expects($this->once())
            ->method('validate')
            ->will($this->throwException(new InvalidCollectionArgumentException('Failed', array())));

        $subject = $this->getMockBuilder(__NAMESPACE__ . '\CollectionType')
            ->disableOriginalConstructor()
            ->setMethods(array('__construct'))
            ->getMock();
        $validatorProperty = new \ReflectionProperty($subject, 'validator');
        $validatorProperty->setAccessible(true);
        $validatorProperty->setValue($subject, $validatorMock);

        $constraintsProperty = new \ReflectionProperty($subject, 'constraints');
        $constraintsProperty->setAccessible(true);
        $constraintsProperty->setValue($subject, array());

        $subject->validate(array('a', 'b', 'c'));
    }
}

<?php

namespace BlueM\Validation\Type;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Tests BlueM\Validation\Type\MonthType.
 *
 * These are not real unit tests (as the class rather contains configuration than code),
 * but tests the functionality of all the parts: type, transformers and constraints.
 *
 * @covers BlueM\Validation\Type\MonthType
 */
class MonthTypeTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
	 * @test
	 */
	public function aValidMonthTypeWithTrailingWhitespaceIsProcessedCorrectly()
	{
        $subject = new MonthType();
        $this->assertSame('2014-02', $subject->validate('02/2014 '));
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 8
     */
	public function aNonScalarValueThrowsAnException()
	{
        $subject = new MonthType();
        $subject->validate(array());
	}
}

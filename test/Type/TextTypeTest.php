<?php

namespace BlueM\Validation\Type;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Tests BlueM\Validation\Type\TextType
 *
 * This is not a real unit test (as the class rather contains configuration than code),
 * but tests the functionality of all the parts: type, transformers and constraints.
 *
 * @covers BlueM\Validation\Type\TextType
 */
class TextTypeTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
     * @test
     */
    public function aValidTextTypeIsProcessedCorrectly()
    {
        $subject = new TextType();
        $this->assertSame('Hello world', $subject->validate('Hello world'));
    }

    /**
     * @test
     */
    public function leadingAndTrailingWhitespaceIsTrimmed()
    {
        $subject = new TextType();
        $this->assertSame('Hello world', $subject->validate(' Hello world '));
    }

    /**
     * @test
     */
    public function verticalWhitespaceIsNormalized()
    {
        $subject = new TextType();
        $this->assertSame("Foo\nbar\nbaz", $subject->validate("Foo\nbar\r\nbaz"));
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 8
     */
    public function aNonScalarValueThrowsAnException()
    {
        $subject = new TextType();
        $subject->validate(array(), false, null);
    }
}

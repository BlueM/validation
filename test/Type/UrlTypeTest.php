<?php

namespace BlueM\Validation\Type;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Tests BlueM\Validation\Type\UrlType
 *
 * This is not a real unit test (as the class rather contains configuration than code),
 * but tests the functionality of all the parts: type, transformers and constraints.
 *
 * @covers BlueM\Validation\Type\UrlType
 */
class UrlTypeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
     * @test
     */
    public function aValidHttpUrlTypeIsProcessedCorrectly()
    {
        $subject = new UrlType();
        $this->assertSame(
            'http://www.example.com/abc/?d=e#fgh',
            $subject->validate('http://www.example.com/abc/?d=e#fgh')
        );
    }

    /**
     * @test
     */
    public function aValidHttpUrlTypeWithPortNumberIsProcessedCorrectly()
    {
        $subject = new UrlType();
        $this->assertSame(
            'http://example.com:80/#',
            $subject->validate('http://example.com:80/#')
        );
    }

    /**
     * @test
     */
    public function aValidHttpsUrlTypeIsProcessedCorrectly()
    {
        $subject = new UrlType();
        $this->assertSame(
            'https://www.example.com/#',
            $subject->validate('https://www.example.com/#')
        );
    }

    /**
     * @test
     */
    public function theHttpSchemeIsAddedImplicitlyIfMissingAndTheUrlTypeStartsWithWww()
    {
        $subject = new UrlType();
        $this->assertSame(
            'http://www.example.com',
            $subject->validate('www.example.com')
        );
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 20
     */
    public function aValidUrlTypeWithMoreThan150CharactersThrowsAnException()
    {
        $subject = new UrlType();
        $subject->validate('http://www.example.com/' . str_repeat('x', 128));
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 19
     */
    public function anInvalidUrlTypeThrowsAnException()
    {
        $subject = new UrlType();
        $subject->validate('http://example');
    }
}

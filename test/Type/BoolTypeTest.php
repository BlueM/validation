<?php

namespace BlueM\Validation\Type;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Tests BlueM\Validation\Type\BoolType. This is not a real unit test (as the class rather
 * contains configuration than code), but tests the functionality of all the parts:
 * type, transformers and constraints.
 *
 * @covers BlueM\Validation\Type\BoolType
 */
class BoolTypeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function creatingAnInstanceCreatedTheExpectedInstanceVariables()
    {
        $subject = new BoolType();
        $stepsProperty = new \ReflectionProperty($subject, 'steps');
        $stepsProperty->setAccessible(true);
        $steps = $stepsProperty->getValue($subject);

        $this->assertInternalType('array', $steps);
        $steps = array_values($steps);

        $this->assertInstanceOf('BlueM\Validation\Constraint\Scalar', $steps[0]);
        $this->assertInstanceOf('BlueM\Validation\Transformer\BoolTransformer', $steps[1]);
    }
}

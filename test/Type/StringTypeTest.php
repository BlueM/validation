<?php

namespace BlueM\Validation\Type;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Tests BlueM\Validation\Type\StringType
 *
 * This is not a real unit test (as the class rather contains configuration than code),
 * but tests the functionality of all the parts: type, transformers and constraints.
 *
 * @covers BlueM\Validation\Type\StringType
 */
class StringTypeTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
	 * @test
	 */
	public function aValidStringTypeIsProcessedCorrectly()
	{
        $subject = new StringType();
        $this->assertSame('Abc', $subject->validate('Abc'));
	}

	/**
	 * @test
	 */
	public function aMultilineStringTypeIsTransformedToASingleLineStringType()
	{
        $subject = new StringType();
        $this->assertSame('ab c d', $subject->validate("ab\nc\r\nd"));
	}

	/**
	 * @test
	 */
	public function theStringTypeIsTrimmed()
	{
        $subject = new StringType();
        $this->assertSame('xy', $subject->validate(" xy "));
	}

	/**
	 * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 8
     */
	public function aNonScalarValueThrowsAnException()
	{
        $subject = new StringType();
        $subject->validate(array());
	}
}

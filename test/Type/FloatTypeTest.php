<?php

namespace BlueM\Validation\Type;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Tests BlueM\Validation\Type\FloatType. This is not a real unit test (as the class rather
 * contains configuration than code), but tests the functionality of all the parts:
 * type, transformers and constraints.
 *
 * @covers BlueM\Validation\Type\FloatType
 */
class FloatTypeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 8
     */
    public function aNonScalarValueThrowsAnException()
    {
        $subject = $this->createSubject('.', ',');
        $subject->validate(array(), false, null);
    }

    /**
     * @test
     */
    public function aValidFloatTypeIsProcessedCorrectly()
    {
        $subject = $this->createSubject('.', ',');
        $this->assertSame(123.45, $subject->validate('123.45', false, null));
    }

    /**
     * @test
     */
    public function aValidFloatTypePaddedWithWhitespaceIsProcessedCorrectly()
    {
        $subject = $this->createSubject('.', ',');
        $this->assertSame(123.45, $subject->validate('  123.45  ', false, null));
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 7
     */
    public function anInvalidFloatTypeThrowsAnException()
    {
        $subject = $this->createSubject('.', ',');
        $subject->validate('1 2 3', false, null);
    }

    /**
     * @param string $decSep
     * @param string $thsSep
     *
     * @return \BlueM\Validation\Constraint\FloatType
     */
    protected function createSubject($decSep, $thsSep)
    {
        $i18nMock = $this->getMockBuilder('BlueM\Validation\I18n')
            ->setMethods(array('getDecimalSeparator', 'getThousandsSeparator'))
            ->getMockForAbstractClass();
        $i18nMock->expects($this->once())
            ->method('getDecimalSeparator')
            ->will($this->returnValue($decSep));
        $i18nMock->expects($this->once())
            ->method('getThousandsSeparator')
            ->will($this->returnValue($thsSep));

        return new FloatType($i18nMock, true);
    }
}

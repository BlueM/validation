<?php

namespace BlueM\Validation\Type;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Tests BlueM\Validation\Type\JsonType
 *
 * These are not unit tests (as the class rather contains configuration than code),
 * but tests the functionality of all the parts: type, transformers and constraints.
 *
 * @covers BlueM\Validation\Type\JsonType
 */
class JsonTypeTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
     * Tears down the fixture
     */
    public function tearDown()
    {
        unset($this->i18n);
    }

    /**
     * @test
     */
    public function validJsonTypeIsProcessedCorrectly()
    {
        $subject = new JsonType();
        $this->assertSame(
            '[{"a": "A"}, "Foo", [3, 9, 27]]',
            $subject->validate('[{"a": "A"}, "Foo", [3, 9, 27]]')
        );
    }

    /**
     * @test
     */
    public function leadingAndTrailingWhitespaceIsTrimmed()
    {
        $subject = new JsonType();
        $this->assertSame(
            '[{"a": "A"}, "Foo", [3, 9, 27]]',
            $subject->validate(' [{"a": "A"}, "Foo", [3, 9, 27]] '."\n")
        );
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 26
     */
    public function textWhichIsNotValidJsonTypeThrowsAnException()
    {
        $subject = new JsonType();
        $subject->validate('["a":"b"]');
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 8
     */
    public function aNonScalarValueThrowsAnException()
    {
        $subject = new JsonType();
        $subject->validate(array());
    }
}

<?php

namespace BlueM\Validation\Type;

require_once __DIR__ . '/../bootstrap.php';

/**
 * Tests BlueM\Validation\Type\XmlType
 *
 * This is not a real unit test (as the class rather contains configuration than code),
 * but tests the functionality of all the parts: type, transformers and constraints.
 *
 * @covers BlueM\Validation\Type\XmlType
 */
class XmlTypeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \BlueM\Validation\I18n
     */
    protected $i18n;

    /**
     * Sets up the fixture
     */
    public function setUp()
    {
        $this->i18n = $this->getMockForAbstractClass('BlueM\Validation\I18n');
    }

    /**
     * @test
     */
    public function validXmlTypeIsProcessedCorrectly()
    {
        $subject = new XmlType();
        $this->assertSame(
            '<root><a>Hello world</a><foo bar="1" /></root>',
            $subject->validate('<root><a>Hello world</a><foo bar="1" /></root>')
        );
    }

    /**
     * @test
     */
    public function leadingAndTrailingWhitespaceIsTrimmed()
    {
        $subject = new XmlType();
        $this->assertSame(
            '<root><a>Foo</a></root>',
            $subject->validate(" <root><a>Foo</a></root>\n")
        );
    }

    /**
     * @test
     */
    public function verticalWhitespaceIsNormalized()
    {
        $subject = new XmlType();
        $this->assertSame(
            "<element>Foo\nbar\nbaz</element>",
            $subject->validate("<element>Foo\nbar\r\nbaz</element>")
        );
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 25
     */
    public function textWhichIsNotWellformedThrowsAnException()
    {
        $subject = new XmlType();
        $subject->validate('<root>Foo</Root>');
    }

    /**
     * @test
     * @expectedException \BlueM\Validation\ValidationFailedException
     * @expectedExceptionCode 8
     */
    public function aNonScalarValueThrowsAnException()
    {
        $subject = new XmlType();
        $subject->validate(array());
    }
}

<?php

namespace BlueM\Validation;

require_once __DIR__.'/bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Constraint
 *
 * @covers BlueM\Validation\Constraint
 */
class ConstraintTest extends \PHPUnit_Framework_TestCase
{

	/**
	 * @test
	 */
	public function byDefaultAConstraintSkipsCheckingOfBlankValues()
	{
        $subject = $this->getMockForAbstractClass(__NAMESPACE__ . '\\Constraint');
        $this->assertTrue($subject->skipsBlankValues());
	}
}

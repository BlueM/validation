<?php

namespace Bluem\Validation;

require_once __DIR__ . '/bootstrap.php';

/**
 * Unit tests for Bluem\Validation\InvalidCollectionArgumentException
 *
 * @covers Bluem\Validation\InvalidCollectionArgumentException
 */
class InvalidCollectionArgumentExceptionTest extends \PHPUnit_Framework_TestCase
{

	/**
	 * @test
	 */
	public function theConstructorSavesTheArrayOfErrorsGivenAsSecondArgument()
	{
        $errors  = array('Error 1', 'Error 2');
        $subject = new InvalidCollectionArgumentException('My message', $errors);

        $errorsProperty = new \ReflectionProperty($subject, 'errors');
        $errorsProperty->setAccessible(true);

        $this->assertSame($errors, $errorsProperty->getValue($subject));
	}

	/**
	 * @test
	 */
	public function getErrors_returns_the_errors()
	{
        $subject = new InvalidCollectionArgumentException('My message', array());

        $errors = array('Error 1', 'Error 2');

        $errorsProperty = new \ReflectionProperty($subject, 'errors');
        $errorsProperty->setAccessible(true);
        $errorsProperty->setValue($subject, $errors);

        $this->assertSame($errors, $subject->getErrors());
	}
}

<?php

namespace BlueM\Validation;

require_once __DIR__.'/bootstrap.php';

/**
 * Unit tests for BlueM\Validation\I18n
 *
 * @covers BlueM\Validation\I18n
 */
class I18nTest extends \PHPUnit_Framework_TestCase
{

	/**
	 * @test
	 */
	public function theConstructorSavesCustomStringsPassedAsArgument()
	{
        $customStrings = array('foo' => 'bar');
        $subject = $this->getMockBuilder(__NAMESPACE__ . '\\I18n')
            ->setConstructorArgs(array($customStrings))
            ->getMockForAbstractClass();
        $customStringsProperty = new \ReflectionProperty($subject, 'customStrings');
        $customStringsProperty->setAccessible(true);
        $actual = $customStringsProperty->getValue($subject);
        $this->assertSame($customStrings, $actual);
    }

	/**
	 * @test
	 */
	public function getTheThousandsSeparator()
	{
        $subject = $this->getMockForAbstractClass(__NAMESPACE__ . '\\I18n');
        $this->assertSame(',', $subject->getThousandsSeparator());
    }

	/**
	 * @test
	 */
	public function getTheDecimalSeparator()
	{
        $subject = $this->getMockForAbstractClass(__NAMESPACE__ . '\\I18n');
        $this->assertSame('.', $subject->getDecimalSeparator());
    }

	/**
	 * @test
	 */
	public function getTheDateInputFormat()
	{
        $subject = $this->getMockForAbstractClass(__NAMESPACE__ . '\\I18n');
        $this->assertSame('m/d/Y', $subject->getDateFormat());
    }

	/**
	 * @test
	 */
	public function onlyUponFirstCallToStringMethodTheStringsAreLoaded()
	{
        $subject = $this->getMockForAbstractClass(__NAMESPACE__ . '\\I18n');
        $subject->expects($this->once())
            ->method('getStrings')
            ->with()
            ->will($this->returnValue(array('key' => 'Translation')));

        $subject->getString('foo');
        $subject->getString('foo');
        $subject->getString('foo');
    }

	/**
	 * @test
	 */
	public function theLocalizationStringIsReturnedIfThereIsNoTranslation()
	{
        $subject = $this->getMockForAbstractClass(__NAMESPACE__ . '\\I18n');
        $subject->expects($this->once())
            ->method('getStrings')
            ->with()
            ->will($this->returnValue(array()));

        $this->assertSame('nonexistent', $subject->getString('nonexistent'));
    }

	/**
	 * @test
	 */
	public function theTranslationIsReturnedIfThereIsATranslation()
	{
        $subject = $this->getMockForAbstractClass(__NAMESPACE__ . '\\I18n');
        $subject->expects($this->once())
            ->method('getStrings')
            ->with()
            ->will($this->returnValue(array('key' => 'Translation')));

        $this->assertSame('Translation', $subject->getString('key'));
    }

	/**
	 * @test
	 */
	public function thePlaceholderValueIsInsertedIntoTheTranslationIfPresent()
	{
        $subject = $this->getMockForAbstractClass(__NAMESPACE__ . '\\I18n');
        $subject->expects($this->once())
            ->method('getStrings')
            ->with()
            ->will($this->returnValue(array('key' => 'Translation %s')));

        $this->assertSame('Translation foo', $subject->getString('key', 'foo'));
    }
}

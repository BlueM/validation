<?php

namespace BlueM\Validation;

require_once __DIR__.'/bootstrap.php';

/**
 * Unit tests for BlueM\Validation\AnnotationReaderTest
 *
 * @covers BlueM\Validation\AnnotationReaderTest
 */
class AnnotationReaderTest extends \PHPUnit_Framework_TestCase
{

    /**
     * This comment contains "@validation-mandatory", but this should be ignored
     *
     * @type string
     *
     * @validation-type string
     * @validation-mandatory
     * @validation-maxlength 20
     */
    protected $property1;

	/**
	 * @test
	 */
	public function getAllOfAPropertiesValidationAnnotations()
	{
        $rp = new \ReflectionProperty(__CLASS__, 'property1');

        $constraints = AnnotationReader::getConstraints($rp);

        $this->assertInternalType('array', $constraints);
        $this->assertSame(
            array(
                'type'      => 'string',
                'mandatory' => true,
                'maxlength' => '20',
            ),
            $constraints
        );
    }

	/**
	 * @test
	 */
	public function getTheValueOfASpecificValidationAnnotation()
	{
        $rp = new \ReflectionProperty(__CLASS__, 'property1');

        $value = AnnotationReader::getConstraint($rp, 'maxlength');

        $this->assertInternalType('string', $value);
        $this->assertSame('20', $value);
    }
}

<?php

namespace BlueM\Validation;

require_once __DIR__.'/bootstrap.php';

/**
 * Unit tests for BlueM\Validation\Type
 *
 * @covers BlueM\Validation\Type
 */
class TypeTest extends \PHPUnit_Framework_TestCase
{

	/**
	 * @test
	 */
	public function addAConstraint()
	{
        $transformerMock = $this->getMockBuilder('BlueM\Validation\Constraint')
            ->setConstructorArgs(array())
            ->getMock();

        $subject = $this->getMockForAbstractClass(__NAMESPACE__ . '\Type');

        $reflm = new \ReflectionMethod($subject, 'addConstraint');
        $reflm->setAccessible(true);
        $reflm->invoke($subject, $transformerMock);

        $stepsProperty = new \ReflectionProperty($subject, 'steps');
        $stepsProperty->setAccessible(true);
        $actual = $stepsProperty->getValue($subject);

        $this->assertSame(array($transformerMock), array_values($actual));
    }

	/**
	 * @test
	 */
	public function addATransformer()
	{
        $transformerMock = $this->getMockBuilder('BlueM\Validation\Transformer')
            ->setConstructorArgs(array())
            ->getMock();

        $subject = $this->getMockForAbstractClass(__NAMESPACE__ . '\Type');

        $reflm = new \ReflectionMethod($subject, 'addTransformer');
        $reflm->setAccessible(true);
        $reflm->invoke($subject, $transformerMock);

        $stepsProperty = new \ReflectionProperty($subject, 'steps');
        $stepsProperty->setAccessible(true);
        $actual = $stepsProperty->getValue($subject);

        $this->assertSame(array($transformerMock), array_values($actual));
    }
}

<?php

namespace BlueM\Validation\I18n;

use BlueM\Validation\ValidationFailure;

require_once __DIR__.'/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\I18n\De
 *
 * @covers BlueM\Validation\I18n\En
 */
class EnTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @test
	 */
	public function getTheStrings()
	{
        $subject = new En();

        $reflm = new \ReflectionMethod($subject, 'getStrings');
        $reflm->setAccessible(true);
        $strings = $reflm->invoke($subject);

        $this->assertInternalType('array', $strings);
        $this->assertArrayHasKey(ValidationFailure::NOT_NUMBER, $strings);
        $this->assertSame(
            'This must be a number.',
            $strings[ValidationFailure::NOT_NUMBER]
        );
	}
}

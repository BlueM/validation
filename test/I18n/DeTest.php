<?php

namespace BlueM\Validation\I18n;

use BlueM\Validation\ValidationFailure;

require_once __DIR__.'/../bootstrap.php';

/**
 * Unit tests for BlueM\Validation\I18n\De
 *
 * @covers BlueM\Validation\I18n\De
 */
class DeTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * @test
	 */
	public function getTheStrings()
	{
        $subject = new De();

        $reflm = new \ReflectionMethod($subject, 'getStrings');
        $reflm->setAccessible(true);
        $strings = $reflm->invoke($subject);

        $this->assertInternalType('array', $strings);
        $this->assertArrayHasKey(ValidationFailure::NOT_NUMBER, $strings);
        $this->assertSame(
            'Dies muss eine Zahl sein.',
            $strings[ValidationFailure::NOT_NUMBER]
        );
	}
}

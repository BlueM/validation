<?php

namespace BlueM\Validation;

require_once __DIR__.'/bootstrap.php';

/**
 * Unit tests for BlueM\Validation\ValidationFailedException
 *
 * @covers BlueM\Validation\ValidationFailedException
 */
class ValidationFailedExceptionTest extends \PHPUnit_Framework_TestCase
{

	/**
	 * @test
	 */
	public function createAConstraintExceptionAndCallGetters()
	{
        $exc = new ValidationFailedException(12345, 'Placeholder value');
        $this->assertInstanceOf(__NAMESPACE__ . '\ValidationFailedException', $exc);
        $this->assertSame('Validation failed', $exc->getMessage());
        $this->assertSame(12345, $exc->getCode());
        $this->assertSame('Placeholder value', $exc->getPlaceholderValue());
	}
}
